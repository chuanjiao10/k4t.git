﻿

# k4t常见问题

------

## 问：每个寿报中有几个容器？

答：1个。

------

## 问：master，node可以安装在同一台机子上吗？

答：不可以。

## 问：k4t元数据从master推送到node完毕后。也不想更新元数据，也不想停止应用，只想维持现有应用。那么k4t的master端，可以关机吗？
答：可以。以后想【更新元数据】，【新建应用】，【删除应用】时，再开机master即可。

------

## 问：我的容器启动较慢（比如redis），如何预热？

答：k4t寿报启动后，就要进行探活操作。使用较慢的探活，和允许更多的探活出错次数即可。具体是修改这个脚本：

c:\ProgramData\kasini3000\k4t_master\shoubao_server\【你的应用名】\running_check\url探活慢.ps1

也可以自己写脚本逻辑，嵌入到探活主脚本【rc.ps1】中。

------

## 问：寿报的cpu限制，内存限制，提供热更新吗？

答：提供。

------

## 问：寿报的环境变量，提供热更新吗？

答：

k4t不提供寿报的环境变量热更新。要想更新环境变量，需要：1编辑node上寿报配置文件，写入环境变量。2在node上，触发寿报滚动更新，这将重启所有容器。

用户可以自行用docker的exec，去更改单个container的环境变量设定。但重启container后，设定的环境变量会丢失。

------

## 问：使用vlan后，寿报的ip可以相同吗？

答：不可以！相同的ip，无法注册url，反向代理。

------

## 问：应该为k4t配哪种ip？

答：

* k4t第一版只支持ipv4，未来将支持ipv6，具体向作者咨询。

* k4t默认支持内网ip，外网ip。内网ip安全，外网ip有被攻击的奉献。

------

## 问：容器网络故障，如何查看修复？

答：

检查用： sysctl net.ipv4.ip_forward

设定用：

在/etc/sysctl.conf配置中配置net.ipv4.ip_forward = 1
systemctl restart docker

------

## 问：k4t如何推倒了一座容器大山（存储）？

问： 如何在容器中使用nfs？

我在容器中这样用，但报错。mount server:/dir /mount/point

答：

在运行的容器内，使用mount，您需要具有此CAP_SYS_ADMIN功能，Docker在创建容器时会丢弃该功能。

有几种解决方案：

1 用--cap-add sys_admin标志启动容器。
这将导致Docker保留该CAP_SYS_ADMIN功能，从而使您可以从容器内挂载NFS共享。
这可能是一个安全问题；不要在不受信任的容器中执行此操作。

2 在主机上挂载NFS共享，并将其作为主机卷传递到容器中：

you@虚拟机 > mount server:/dir /path/to/mount/point
you@虚拟机 > docker run -v /path/虚拟机路径:/path/容器内路径

3 使用Docker卷插件（如Netshare插件）直接将NFS共享作为容器卷挂载：
you@host > docker run \
–volume-driver=nfs \
-v server/dir:/path/to/mount/point


------



------



------



------



------



------



