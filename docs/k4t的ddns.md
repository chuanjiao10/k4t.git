﻿
------

# 什么是k4t的 DDNS？

k4t集群使用 DDNS 进行服务发现。

可以使用您所熟悉的软件，作为ddns。

# 如何注册，反注册？

以win为例：

## 应用级
主控机c:\ProgramData\kasini3000\k4t_master\shoubao_server\你的应用名\dns_register\reg.ps1
主控机c:\ProgramData\kasini3000\k4t_master\shoubao_server\你的应用名\dns_unregister\unreg.ps1

## 系统级
主控机c:\ProgramData\kasini3000\k4t_master\default\dns_register\reg.ps1
主控机c:\ProgramData\kasini3000\k4t_master\default\dns_unregister\unreg.ps1

------

# 关于win2019

win2019内置dns 是一个灵活可扩展的 DNS 服务器，可以作为 k4t 集群的 DDNS。

# 安装 k4t 的dns

## 一安装win2019

先安装有图版，熟练后安装无图版，无图版win+dnsd总共占用内存800mb左右。可以不用激活。不激活使用半年，半年后重装。

## 二从客户机管理win2019

有图版win2019管理略。

无图版win2019，可以通过在win10上安装WindowsAdminCenter，然后安装wac的dns插件，然后添加win2019服务器的ip，然后从客户端用wac的web图形管理win2019。

无图版win2019，还可以通过在win10上通过powershell远程管理。

### win10启用本地winrm：
Enable-PSRemoting

### 从客户机，信任服务器
Set-Item WSMan:\localhost\Client\TrustedHosts * -Force

### 从本地win，连接远程win2019
Enter-PSSession 远程ip -Credential (Get-Credential)


### 在远程win2019上，更改ip。更改成功后会卡死，需要重新用新ip连接win2019。

 (Get-NetAdapter)[-1] | New-NetIPAddress -AddressFamily IPv4 -IPAddress 192.168.11.220 -PrefixLength 8

### 设定dns

 (Get-NetAdapter)[-1] | Set-DNSClientServerAddress -ServerAddresses 192.168.11.1,114.114.114.114


### 图形安装dns服务器：

1 dns服务器
2 远程服务器管理工具---》角色管理工具---》adds工具，和adlds工具---》dns服务器工具


### 对于无图版win2019，用下列powershell命令远程安装dns服务器：
远程登录到win2019后用：
Install-WindowsFeature dns,RSAT-DNS-Server


## 三用powershell命令管理win2019的dns域

获取dns域信息：
```
Get-DnsServerZone
Get-DnsServerResourceRecord -ZoneName v1.k4t
```

创建dns域：
```
Add-DnsServerPrimaryZone -Name v1.k4t -ZoneFile v1.k4t.dns
```

添加主机和ip：
```
Add-DnsServerResourceRecordA -Name ng -IPv4Address 192.168.12.11 -ZoneName v1.k4t -TimeToLive 00:03:00
Add-DnsServerResourceRecordA -Name ng -IPv4Address 192.168.12.12 -ZoneName v1.k4t -TimeToLive 00:03:00
```

从客户机查询：
```
Resolve-DnsName ng.v1.k4t -Type a -Server 192.168.11.220
```

删除主机和ip：
```
Remove-DnsServerResourceRecord -Name ng -RecordData '192.168.12.11' -ZoneName v1.k4t -RRType A   -Force
```

## 四用win2019上的dns，作为k4t的名字服务。

### 原理：应用内的容器启动成功后，会运行这个文件:
被控机/etc/kasini3000/k4t_node/shoubao/你的应用名/dns_register/reg.ps1
然后运行
被控机/etc/kasini3000/k4t_node/_建立dns_session文件.ps1
然后向win2019的dns服务器上注册【应用名】【应用ip】dns的A记录。


* k4t的应用名，为1-16个英文或汉字，可以作为k4t的域名的主机名部分，支持汉字。

* 域名为v1.k4t。

* 支持ipv4的ip。原则上支持ipv6的ip，但需要修改下这2个脚本：
【主控机c:\ProgramData\kasini3000\k4t_master\default\dns_register\reg.ps1】
【主控机c:\ProgramData\kasini3000\k4t_master\default\dns_unregister\unreg.ps1】

* 在win2019上运行【Add-DnsServerPrimaryZone -Name v1.k4t -ZoneFile v1.k4t.dns】

* 把这两个文件中的【return $true】注释掉：
c:\ProgramData\kasini3000\k4t_master\shoubao_server\你的应用名\dns_register\reg.ps1
c:\ProgramData\kasini3000\k4t_master\shoubao_server\你的应用名\dns_unregister\unreg.ps1

* 在每台node中，更改这个文件：
/etc/kasini3000/k4t_node/_建立dns_session文件.ps1
更改其中的：

```
$private:PSRemoting服务器ip = '1.2.3.4'
$private:密码明文 = '你的密码'
```

改成win2019的dns服务器的ip和密码。

## 测试：

假设你的应用名为【张三】，寿报启动成功后 ：
```
Resolve-DnsName 张三.v1.k4t -Type a -Server 你的win2019dns服务器ip地址
```
会返回n个a记录。每个ip，对应一个容器。当容器停止，则ip被删除。

------

# 谢谢观看（完）


