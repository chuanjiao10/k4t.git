﻿
------

# 版本分类

群名称：k4t官方群（国产k8s）
群   号：722528388

* 全功能旗舰版。需要进入本项目的官方群。必须经过培训后，获得。目前收培训费100元。

* 标准版。需要进入本项目的官方群。必须经过收费培训后，获得。目前免费。

* 雏形版。开源免费，随便下载。

------

# 旗舰版，master端安装：

k4t的master依赖于卡死你3000。
1需要安装卡死你3000。
2需要编辑nodelist.csv文件，以添加被控机。
3需要连通被控机测试。


------

# 旗舰版，master端功能：

## 应用排斥另一个应用。

应用对另一个应用的排斥，记录在应用元数据中。

1 在master上，运行这个脚本：
```
c:\ProgramData\kasini3000\k4t_master\pcadd_node_本应用_添加对其他应用的排斥.ps1 -应用的uuid xxx -应用名 xxx
c:\ProgramData\kasini3000\k4t_master\pcdel_node_本应用_删除对其他应用的排斥.ps1 -应用的uuid xxx -应用名 xxx
c:\ProgramData\kasini3000\k4t_master\pcget_node_本应用_获取对其他应用的排斥.ps1
```

2 推送到node。

## 存储，推送.tar格式的image文件功能。

* k4t拥有从主控机推送tar格式的，image文件功能。

```
c:\ProgramData\kasini3000\k4t_master\shoubao_server\应用名\image
或
/etc/kasini3000/k4t_master/shoubao_server/应用名/image
```


* 也可以编写脚本，从nfs拉取tar格式的镜像。

编辑这2个脚本，就可以触发docker login。也可以用来它实现，1mount局域网nfs，2从nfs复制tar，3umount。

```
/etc/kasini3000/k4t_node/应用名/pull_image_before/docker_login.ps1
/etc/kasini3000/k4t_node/应用名/pull_image_after/docker_logout.ps1
```

* 从被控机，用docker pull下载镜像。

## b

## c


## 问：k4t元数据从master推送到node完毕后。也不想更新元数据，也不想停止应用，只想维持现有应用。那么k4t的master端，可以关机吗？
答：可以。以后想【更新应用】，【新建应用】，【删除应用】，【移动应用】时，再开机master即可。

------

# 关于旗舰版，node端，多个ip地址：

要实现k4t的最基本功能，需要在node上建立多个ip地址，形成【ip地址池】。
这个步骤，有专门的linux配套脚本实现。可以一次建立1---750个ip。
可以参考下面，旗舰版node安装教程。


## 建立两个ip。

192.168.11.11
192.168.11.12

然后在node上的/etc/kasini3000/k4t_node/ip_pool/目录中，

## 建立两个文本。

192.168.11.11.ipv4
192.168.11.12.ipv4

## 测试：

从k4t的master：
ping 192.168.11.11
ping 192.168.11.12

------

# 旗舰版，node端安装：

https://gitee.com/chuanjiao10/k4t/blob/master/docs/node%E5%AE%89%E8%A3%85_cn.md

------

# 旗舰版，node端功能：

k4t的node端有如下功能：

## 每node不同的，最小保留cpu，最小保留内存。

```
/etc/kasini3000/k4t_node/k4t_node_config.ps1
```

## 下载镜像

* 根据推送过来的元数据文件。从被控机，用docker pull下载镜像。

## tar镜像导入

k4t具有镜像导入到node的功能。这是自动的。

应用名为【abc】的image目录下，有一个任何【.tar】，都会被自动导入。

## 延时滚动更新。

1 在master上更改元数据文件。没应用可以设定滚动更新间隔秒。

2 推送到node。即可从主控机触发滚动更新。

3 暂定：每个node，每次只能滚动更新一个容器。而不是批量。

## 立即重启，应用内的所有容器。此功能已经实现  -----对应滚动更新。

## node排斥应用。

运行这个脚本，就可以触发应用排斥。

```
/etc/kasini3000/k4t_node/____addp给linux的node新增排斥一种应用.ps1 -应用的uuid xxx
/etc/kasini3000/k4t_node/____delp给linux的node删除已经排斥的应用.ps1 -应用的uuid xxx
```

应该在master上，远程调用这个脚本。


## 启动应用：

linux的node上的crontab，会启动powershell，会启动k4t客户端引擎（ps1脚本）
k4t客户端引擎，会定时扫描【/etc/kasini3000/k4t_node/shoubao】目录。
也就是说，只要我们把应用【abc】，从master推送到node。若干秒后，引擎会自动启动应用【abc】的两个容器。

## 触发停用应用。

运行这个脚本，就可以触发应用停止。

```
/etc/kasini3000/k4t_node/ty_node_添加寿报停用标记.ps1  -应用名 abc
```

这是优雅停止容器，
它首先遵守应用配置文件中写的【$寿报下岗前_等待时间秒】，若值为空，则遵守【$global:寿报下岗前_等待时间秒_全局】
然后摘除dns，摘除反向代理（负载均衡）。
最后停止容器。

## 触发启用应用。

运行这个脚本，就可以触发应用启用。

```
/etc/kasini3000/k4t_node/qy_node_添加寿报启用标记.ps1  -应用名 abc
```

## 检查应用状态：

```
/etc/kasini3000/k4t_node/jc_node_检查寿报状态.ps1 -应用名 abc
```

## 在node上添加污点，删除污点。

运行这个脚本，就可以给node添加污点，删除污点。

```
/etc/kasini3000/k4t_node/____addw给linux的node建立污点.ps1
/etc/kasini3000/k4t_node/____delw给linux的node删除污点.ps1
```

应该在master上，远程调用这个脚本。

## 在node上添加驱逐点，删除驱逐点。

node上运行这个脚本，就可以给node添加驱逐点，删除驱逐点。

```
/etc/kasini3000/k4t_node/____addq给linux的node建立驱逐点.ps1
/etc/kasini3000/k4t_node/____delq给linux的node删除驱逐点.ps1
```

应该在master上，远程调用这个脚本。

## 在node上添加【排斥某应用】，删除【排斥某应用】。

node上运行这个脚本，就可以给node添加【排斥某应用】，删除【排斥某应用】。

```
/etc/kasini3000/k4t_node/____addp给linux的node新增排斥一种应用.ps1 -应用的uuid xxx
/etc/kasini3000/k4t_node/____delp给linux的node删除已经排斥的应用.ps1 -应用的uuid xxx
```

应该在master上，远程调用这个脚本。


## 测试：

容器启动后，我们可以在【master，node之外的机子上】，
通过浏览器访问 192.168.11.11，      192.168.11.12，或curl访问。

## url探活。

相关脚本在这里。

```
/etc/kasini3000/k4t_node/shoubao/应用名/running_check/rc.ps1 -容器ip地址 '192.168.11.11' -容器端口 80 -http探活路径 '/'
```



------

# 谢谢观看（完）

雏形版只是演示原理，多数功能都被我删除了，无法生产使用。请入群下载标准版，免费哦。

