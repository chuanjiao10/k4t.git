﻿
# 在win，linux，主控机上，安装k4t的服务器端（master端）

----

# master系统需求

## 操作系统需求：

* win：64位的win10，win2016，win2019

* linux：支持x86-64，arm架构。

centos7，centos8，amazon linux 2，麒麟v10高级服务器版x86-64 (Tercel)，统信UOS服务器版2020，Anolis OS 8，ubuntu1604，ubuntu1804，ubuntu2004，debian9，debian10，debian11，alpine3.9---3.13。**不支持centos6**


## 磁盘空间需求：

k4t需要为每一个应用，的每一个node建立一个目录。会建立大量小目录，文件。

对于linux，这需要大量的i-node。

对于win，建议给系统保留大的剩余空间。


## 软件需求：

* powershell v7.0.6及以上，或powershell v7.1.3及以上。不支持powershell 5。不支持powershell 6。

* 卡死你3000 master端软件。

* k4t master端软件。

----

# 一、目的：安装powershell，安装卡死你3000master。

安装卡死你3000主控机

linux:
https://gitee.com/chuanjiao10/kasini3000/wikis/安装linux版卡死你3000主控机

win:
https://gitee.com/chuanjiao10/kasini3000/wikis/安装win版卡死你3000主控机

* 编写主控机上的nodelist.csv文件，添加此被控机ip，端口，账户，密码等。

* 复制主控机上的ssh key file，到linux的node。

* 在主控机上，运行测试命令：

```
cdip 此k4t被控机ip;
krun {ls -l /tmp}
```

----

# 二、目的：安装【k4t master】版。

# 动作：

把从qq群【722528388】共享，获得的压缩包【k4t_master.zip】，解压到这个目录：

/etc/kasini3000/k4t_master
或
c:\ProgramData\kasini3000\k4t_master


----

# 三、目的：启用k4t监控功能。（必须）

k4t监控功能，可以放在单独的虚拟机上。也可以放在master上。

# 作用：监控k4t的node的【存活】，【cpu】，【内存】，【磁盘】，【agent】，【负载均衡器】，【dns】，以触发k4t master动作。

# 动作：

运行这个文件：
2_开启k4t监控功能_建立win任务计划.ps1

或

2_开启k4t监控功能_建立linux_crontab.ps1

----

# 四、目的：

-->
