﻿
------

# 什么是负载均衡网关

 ![api-getway](api-getway.png)

API网关是企业中的关键基础架构组件，它通过一组协议（通常是通过一组RESTful 应用程序编程接口（API）），Web和其他外部客户端提供后端务。

微服务API网关位于微服务的前面，并向包括其他微服务的客户端执行API网关的外部功能。

通常通过将大多数基础架构级别的逻辑从后端微服务和客户端都移到网关中，网关通常使开发，保护，管理和扩展端点变得更加简单。

流量总入口，得以集中控制！

------

# 分类

golang开源api网关
代表有Tky、Manba、GOKU API Gateway、Ambassador(基于Envoy)、Gloo(基于Envoy)、KrakenD、BFE

Java开源api网关
代表有Gravitee、Zuul、Sentinel、MuleSoft、WSO2、Soul

Erlang开源api网关
代表有RIG – Reactive Interaction Gateway

.net开源api网关
代表有Ocelot

Node.js开源api网关
代表有express-gateway

闭源商业api网关
从gartner（艾瑞咨询类似）的权威报告可以找到老牌的api网关玩家是谁

行业老大：Apigee、3Scale、Amazon等
各大云都是玩家，比如阿里云api网关、腾讯云api网关、Amazon API Gateway


------

# 开发者【ps1传教士】的话：

k4t的容器，依赖于微服务网关。传教士称之为“http url跳转工具”。

是k4t并没有自己开发它，而是采用市面上常见的网关。

老型号的工具如nginx，haproxy，因为支持动态配置，需要狂reload，基本已经被淘汰。

新型号的网关，原则上任何都可以，欢迎向我推荐。


------

# envoy-powershell

https://gitee.com/chuanjiao10/envoy_powershell

## 特色

这是一个基于【ssh命令行】开发的开源，免费网关。内部基于envoy。

使用双linux + keepalived ，实现高可用架构。

<!--
------

# apisix


https://gitee.com/iresty/apisix

## 特色

这是一个国产api网关。类似于kong。它是基于etcd+lua+nginx构建。即3台或5台etcd，加1台apisix。

限流，限速，黑白名单，dashboard，灰度发布等功能。

基于http，或curl，提交动态信息。

## 开始使用

https://gitee.com/iresty/apisix/blob/master/docs/zh/latest/getting-started.md
-->

------

# express-gateway

https://www.express-gateway.io/docs/

这是一个基于【express-js】开发的免费网关。绝大多数功能免费提供。

## 特色

https://www.lunchbadger.com/api-gateway-comparison-kong-enterprise-pricing-vs-express-gateway/

## 安装

------

# 云计算 dnat

 ![云dnat](云dnat.jpg)

## 亚马逊aws
https://docs.aws.amazon.com/zh_cn/vpc/latest/tgw/what-is-transit-gateway.html
https://docs.amazonaws.cn/vpc/latest/userguide/VPC_Internet_Gateway.html#api_cli_overview

创建 Internet 网关：New-EC2InternetGateway
删除 Internet 网关：Remove-EC2InternetGateway
将 Internet 网关附加到 VPC：Add-EC2InternetGateway
将 Internet 网关与 VPC 分离：Dismount-EC2InternetGateway


## 微软azure

## 阿里云

## 腾讯云

## 华为云


------

# 谢谢观看（完）


