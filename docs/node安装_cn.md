﻿
# 软件需求：

* linux：支持x86-64，arm架构。

centos7，centos8，amazon linux 2，麒麟v10高级服务器版x86-64 (Tercel)，统信UOS服务器版2020，Anolis OS 8，ubuntu1604，ubuntu1804，ubuntu2004，debian9，debian10，debian11，**暂不支持alpine**   **不支持centos6**

* powershell v7.0.6及以上，或powershell v7.1.3及以上。

* k4t node端软件。

* docker 软件。


----

# 克隆的linux机子的，注意事项：

* 确保每块网卡mac地址不同。 ip addr

* 确保每台机子uuid不同。 cat /sys/class/dmi/id/product_uuid

* 却保ssh主机id不同。 rm -rf /etc/ssh/ssh_host* ; ssh-keygen -A ;ls -la /etc/ssh/*


----

# 一、目的：在linux被控机上，安装【卡死你3000客户端】即powershell。

在被控机上操作：

* linux的node端一键安装powershell。
https://gitee.com/chuanjiao10/kasini3000_agent_linux



在主控机上操作：

* 在卡死你3000主控机上，已经编写好nodelist.csv文件，添加此被控机ip，端口，账户，密码等。

* 复制卡死你3000主控机上的ssh key file，到此linux的node。

* 在卡死你3000主控机上，运行测试命令：

```
cdip 此k4t被控机ip;
krun {ls -l /tmp}
```

----

# 二、目的：在linux被控机上，安装【k4t node软件】。

# 动作：

把从qq群【722528388】共享，获得的压缩包【k4t_node.zip】，解压到这个目录：

/etc/kasini3000/k4t_node

注意：文件名有中文。一定要以utf8编码解压。或传输时选择utf-8编码。

----

# 三、目的：在linux被控机上，安装docker。

# 动作：

运行这些安装脚本之一。

```
___install_docker_centos7_centos8.ps1
___install_docker_debian9_debian10.ps1
___install_docker_ubuntu1604_1804_2004.ps1
```

----

# 四、目的：启用k4t的node上的任务计划。

# 动作：

运行这个文件：
3bkj_linux_install_k4t_node_crontab.ps1

----

# 对于全功能旗舰版用户，提供批量安装脚本。这个脚本可以批量执行“目的2”“目的4”。

## 用法：
```
#把全功能旗舰版，解压到主控机，并在“主控机上”运行这段代码。
$被控机ip = '1.1.1.1','2.2.2.2'
foreach ($i in $被控机ip)
{
	cdip $i
	3zkj_从主控机_到当前ip的被控机_远程安装k4t的clientz.ps1
}
```

----

# 五、目的：给k4t的node机，建立ip地址池：

要实现k4t的最基本功能，需要在 **每台node机子上** 建立多个ip地址，形成【ip地址池】。
这个步骤，有专门的linux配套脚本实现。可以一次建立1---750个ip。

## 5-1 目的：启用networkmanager。（centos7，centos8忽略这个步骤）

这里以ubuntu1804为例。动作：

运行【/etc/kasini3000/k4t_node/k4t_create_ip/ubuntu/1804/ubuntu1804_server_install_networkmanager.ps1】

## 5-2 目的建立ip

```
# 在此k4t被控机上运行
foreach ($i in 1..250)
{
	$ip = "192.168.11.${i}"
	/etc/kasini3000/k4t_node/k4t_create_ip/ubuntu/1804/ubuntu1804_addip.ps1 $ip
	/etc/kasini3000/k4t_node/_addi给linux的node建立ip池中的ip.ps1 $ip
}
```
