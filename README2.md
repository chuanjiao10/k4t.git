﻿# 问：k4t是什么？

答：

k4t（国产k8s）是一套，基于《卡死你3000》打造的，全功能，容器集群的自动化部署、扩容以及运维的平台。

具有网络简单稳定（每容器一个虚拟机级ip），使用简单，脚本操控灵活的特点。

轻量，架构松散极易扩展，可diy。每一个子功能都支持插件，并通过脚本调用，以扩展功能。

号称比k8s简单10倍，功能强1倍，稳定强1倍。

目前基于docker，后续会增加kata-container（虚拟机级别容器）。

k4t对标【docker swarm】，【kubernetes】

kubernetes入门门槛较高，运维复杂。本项目致力于降低安装、运维、使用复杂度。让客户专注于业务，解放生产力。

欢迎给 ★

------

# 项目名称，图标，官方群。

 ![k4t](喵星海贼王3.jpg)

项目中文名：《kaiiit生产级别的容器编排系统》

项目简称： k4t
注：kaiiit是我发明的单词，即cat（猫）。k4t即：'k' + 'k后面有4个字母' + 't'的意思。

中文别名：《ps1倚天剑》 《ps1绣花针》《海贼帝·优秀的怕被沽沟暗杀·黑毛腿》《国产k8s》

作者昵称：逆练k8s的欧阳疯

k4t 官方QQ群号：722528388

群名称：k4t官方1群

这个群目前人不多，以后将人满。将收费入群。

这个项目可以出售，欢迎云厂商，容器厂商与我联系合作。1328486072（我email）qq.com

------

# 问：完美版的k4t项目，由哪几部分组件构成？

# 答：

## 1 master端。

正在开发中。

## 2 node端。

已经开发完成，并部分开源免费成为雏形版。

## 3 动态负载均衡api网关。（外部）

提供反向代理，和负载均衡功能。用于给一组容器应用，形成统一的ip。

目前基于我编写的envoy后端增删脚本。 https://gitee.com/chuanjiao10/envoy_powershell

你也可diy，改成自己喜欢的。

对于公有云：可以用共有云的nat网关代替它。即：

用户---》公有云nat网关---》编写powershell脚本【c:\ProgramData\kasini3000\k4t_master\应用名\url_register\reg.ps1】，调用云命令行，添加DNAT规则（外网弹性ip ---》node上的内网容器ip，192.168.1.2) ---》容器端口上的应用

反向代理和负载均衡，在这种极端情况下，是【非必要组件】：

容器ip，全都用外网ip，而没有内网ip，就可以不需要反向代理网关。 即 客户---》ddns ---》外网ipv4的容器ip（位于node上）。

【所有应用】都已经使用了，下面的ddns，作为容器的负载均衡器。

node上的容器ip都是外网ipv4的ip；

node上的容器ip都是外网ipv6的ip（目前暂不支持v6ip，计划支持）；


## 4 动态dns。（外部 非必要组件）

非必要组件。K4t集群，可以只用负载均衡器 api-getway。

用于给一组容器应用，形成统一的服务名字。

目前基于未激活的无图win2019。当然有图版win2019也行。你也可diy，改成自己喜欢的。例如：dnsmasq


------

# 问：最小规模的k4t项目，由哪几部分组件构成？系统需求如何？

# 答：

1 一台master机。linux内存需求1gb左右，win要2gb左右。win，linux，树莓派都可以。如果规模大，将产生大量小文件，需要磁盘i-node多些。

2 一台node机，或n台。上面安装linux + docker。

3 一台动态dns。内存需求1gb左右。由win2019无图（或有图）。或其他动态dns方式组成。

4 一台动态负载均衡器。内存需求1gb左右。由一台linux机组成。

内部使用1docker，2envoy镜像，3powershell，4《envoy powershell 遥控器》软件。当然也可以把envoy安装在宿主linux。

------

# 问：请简单总述下k4t，对比k8s，有何差别？

# 答：

1 k8s类似于微软活动目录。元数据存储集成化，不透明。

2而k4t类似于dns服务器bind。元数据存储文本化，分布式，透明。
k4t 号称【ps1绣花针】比k8s轻量，简单。


------

# k4t架构图

 ![k4t架构图](k4t架构图.png)



------

# 问：k4t项目，和k8s，和 docker swarm 最大的区别是什么？

# 答：k4t网络部分比k8s，docker swarm简单好几倍。稳定强好几倍。性能强好几倍。 网络打通【容器编排集群】内外。让集群外，可以访问集群内的微服务。

k4t容器不用veth，不用vxlan。不用cni，不用cri。不用calico，不用flannel。

而用：

1 在虚拟机内建立m个网卡。

2 在每个网卡上绑定n个ip。（eth0：1，eth0：2等）

一般来讲，每个node上1---4个虚拟机级别网卡，对应千兆物理网卡。每个虚拟机级别网卡，绑定250个左右ip。node上总不超过1000个ip即可。

这1000个ip，就可以启动1000个容器。

3 在局域网中，用vlan隔离node上的多个ip。最多4000左右个vlan。超过4000，也用【局域网硬件交换机上的vxlan】

注意：硬件交换机上的vxlan，只用于给vlan扩容。

4 未来要实现，ip绑定容器，【容器+ip】在虚拟机之间移动。即【无双寿报】。

------

# 问：完美版的k4t项目，将实现百分之多少，k8s的功能？

# 答： 110%----120%

我认为，我用powershell代码开发了，70%的功能。这些功能是和k8s雷同的。

有10%k8s的功能，被我当糟粕丢掉了。这部分详见主页---》将来应该永远不会有的功能。

有11%的【无双寿报】功能，是我独创发明的。这些功能k8s没有。

有10%的k8s功能，是dns和dns注册。k4t有dns注册接口，没有开发dns功能。而是通过安装，搭建，来实现的。你可以选择不同的dns来搭建。

有10%的k8s功能，是ip负载均衡和ip注册。k4t有ip注册接口，没有开发ip负载均衡功能。而是通过安装，搭建，来实现的。你可以选择不同的【负载均衡器】来搭建。

大概有10%---20% 的功能增强。

# 100% - 10%糟粕 - 10%dns - 10%负载均衡 = 70% + 11%独创的无双寿报代码 + 10%-20%独创增强 = 最终我开发的k4t，相当于101%的k8s功能。剩余20%功能，是用搭建实现的。

# 说明

1 这里说的是【k4t全功能旗舰版】。因为【k4t雏形版】的功能几乎被我删除光了。

2 很多功能还在开发中。并不是说现在已经实现，而是说要项目想要实现。具体来讲，请看旗舰版v1---v4要实现的功能。


------

# 雏形版。 （已经发布。开源+免费）

下载：     git clone https://gitee.com/chuanjiao10/k4t.git

**雏形版功能不全，因为我在里面只放了，少量最简单的功能模块**

雏形版是用来测试，和演示架构原理。鉴于目前总有人质疑k4t，我发布雏形版的目的是：

1演示k4t网络原理。

2演示k4t的三级元数据架构（文件目录）。

感觉k4t靠谱的，请入官方群，找我要 **【标准版】或【旗舰版】** ，必须进行内测培训。

# 雏形版使用教程

https://gitee.com/chuanjiao10/k4t/blob/master/docs/k4t%e9%9b%8f%e5%bd%a2%e7%89%88readme.md


# 雏形版更新日志：

2020-08-05 首次上传代码。

2020-08-27 陆续上传代码。

2020-09-01 发布第一版，雏形版软件。

------

# 标准版。

都是ps1脚本，以源码方式运行。

计划近期发布标准版。本项目官方群内，找我要，免费。

## 标准版，下述功能被我删减掉了。

* 驱逐node上所有应用功能。

* 给应用禁用node。

* 给应用启用node。

* 给应用移动node。

------

# 全功能旗舰版，使用教程：

https://gitee.com/chuanjiao10/k4t/blob/master/docs/k4t旗舰版readme.md

# 全功能旗舰版。项目进度。 roadmap

本项目官方群内，找我要，收少许培训费。都是ps1脚本，以源码方式运行。

## V1

v1具有下述特性内的所有功能，但不含v2，v3，新增功能。


## V2

v2未来将新增：

node根据cpu，内存，的占用率，自动伸缩功能。这是k4t_master的功能。

从主控机到nfs复制文件，从nfs到主控机复制文件。

计划明年发布v2 alphi1版。


## V3

v3未来将新增：“无双寿报”功能。这是k4t_node的功能。

计划未来发布v3 alphi1版。

## V4

v4未来将新增：gpu支持。这是k4t_node的功能。这是k4t_master的功能。

v4未来将新增：虚拟机隔离级别容器【kata-container】支持。这是k4t_node的功能。

计划发布时间：未知。

------

# 项目口号

## 跪拜轻量级的主从架构，唾弃复杂的【拜占庭问题】。主从架构道：“我还不想死，我还能再活500年~”

【拜占庭】是历史真人。【拜占庭问题】是数学家提出的，关于分布式的虚拟问题。

## 拳打k8s，脚踢docker swarm，引领docker使用潮流。

## 每个node可配200---2000个ip。ip还可以绑定容器，ip随容器在node间移动！

## 别去学yaml关键字，k4t让linux的文件目录再次伟大！

![image](ktlp.jpg)


------

# k4t 功能特色

![image](0k4t简介.png)

## 一 、k4t最大的好处：（未来实现）
在k8s集群，或docker swarm上，搭建应用集群，如redis集群，一直比较困难。

其根本原因是在node上暴露端口困难，而nodeport又和内层ip路由纠缠不轻，导致了痛点。Statefulset根本不灵，需要开发xxx-operator解决。

而k4t从根本上就没有这种问题。请记住： **“k4t专为构建container集群应用”打造！k4t专门为解决Statefulset骗局而生** 比如mysql5.6的主从。欢迎用docker构建集群用户使用。

网络打通【容器编排集群】内外。让集群外，可以访问集群内的微服务。

## 二 、元数据存储的特色：（国产etcd）

master上使用【目录+文件】保存元数据。etcd其实也是目录文件。好处是：

1 哪怕元数据的数据库达到100gb，也不占内存。

2 不依赖master。99%基本只需要1个master，而不需要主备高可用。

master挂了1---2分钟无所谓。master重启1---2分钟无所谓。

比如10个node，

那么接上网线10秒，同步数据。断开网线2分钟，数据同步出错。

那么接上网线10秒，同步数据。断开网线2分钟，数据同步出错。

master长时间挂了，毫不影响node上容器的工作，毫不影响网络。

用户 ---》跟k4t无关的外部负载均衡器，或反向代理，或dns ---》node节点

3 高可用的话，需要2个master。

两个master之间同步数据，对于win，linux，可以使用卡死你3000的文件定时复制。

若两个master都是linux，用rsync也行。

结论：

从分布式角度来讲，etcd选择了强一致数据库，而k4t选择了不一致数据库，和间隔20---120秒同步一次数据库。

<!--
主控机上的每个pwsh进程，需要占用80MB---120MB内存，最多开5个进程。

除操作系统占用的内存外，k4t的master最多占用600MB内存。而k8s或etcd做不到！

-->

4 k4t master基本不占用cpu、内存、资源，但占用网络，io资源。每台master带50台node的话，容易之极。带100---200台node是正常。千台是极限。当然这是我设想的，没经过验证。


## 三 、master的特色

* 让k4t管理员控制，寿报负载均衡算法。

* 让k4t管理员控制，寿报负载均衡周期。



## 四 、master和node通信的特色

master-node通信采用ssh协议，而不是https协议，无需制作证书。

无需折腾ca，cn，域名绑定ip等。

没有证书问题。不必受k8s证书1年限制。

只需要制作，分发秘钥对。既简单又安全。谁敢说ssh秘钥对，不如tls证书安全。

而且不需要join master。

ssh客户端和sshd都是现成的，无需重复造轮。

无需另开端口，无需另开放防火墙，无需另起进程监听。

## 五 、 node节点工作特色

node节点上的container容器的工作，依赖【从主控机复制到本地磁盘】上的元数据，而不依赖主控机上的元数据。

master长时间挂了，毫不影响node上容器的工作，毫不影响网络。

用户 ---》跟k4t无关的外部负载均衡器，或反向代理，或dns ---》node节点

k4t默认支持内网ip，外网ip。

## 六 、外部的，dns注册，反向代理，的特色

* 让k4t管理员从外部，通过配置dns，算法，时间。来达到应用限流，控制负载均衡的目的。

* 让k4t管理员从外部，通过配置反向代理，算法，时间。来达到应用限流，控制负载均衡的目的。

## 七 、k4t资源占用低。云计算用k4t，能给你省钱！！！

* master默认会由管理者起1个powershell进程，作为终端。最多再起一个powershell进程，用于多线程推送元数据。总共占用最多不超过400MB内存。

最少的话，100mb左右内存，起1个powershell进程。临时推送完元数据后，后即可退出。

* node默认会起1个powershell进程。占用内存不超过150MB。


------

# k4t功能列表  m=master，n=node。a=应用。 x=貌似k8s没有的新功能。

## master节点功能：

### master节点最主要的功能是： 推送元数据库，负载均衡算法！

* （这个功能k8s应该没有）根据每个应用设定的不同，自动在linux节点机之间，根据cpu，内存监控等结果，负载均衡容器。#'在node上平均寿报数量','优先使用内存低的','优先使用cpu低的','按节点id顺序_让寿报填满cpu内存之一','使用权重百分比','使用node节点上的sc自动伸缩脚本'

k8s在node上负载均衡pod，有一套内置算法。不受用户控制，不能单独对每个servcie设置。

* （这个功能k8s应该没有）允许管理员在master上，手动用命令强制根据cpu，内存监控等结果，负载均衡容器。（因为主动负载均衡，会杀死容器，会导致容器内业务中断，会导致应用网络和功能的波动）

* （这个功能k8s应该没有）部署那些k8s不好支持的，集群应用！

* 选项功能。推送master上的所有元数据，到某个指定的N个node节点。以达到备份元数据的目的。每分钟copy1次，循环copy10个副本。

------

## node节点功能：

* 应用滚动更新

* 驱逐点标记。

* 污点标记。

* node排斥某个应用，删除排斥。这可以理解为临时性排斥某应用。

* （这个功能k8s应该没有）container数量限制

* node最小保留内存_MB = 120

* node最小保留cpu_百分比 = 5

* 支持外网ip。支持内网ip（默认）。对同一个应用，支持部分容器，配外网ip；另一部分容器，配内网ip。比如：

假设应用a，共有5个容器副本。3个调度到node1上，这三个容器拥有外网ip。2个调度到node2上，这两个容器拥有内网ip。

------

## 从master到node功能：

* （这个功能k8s应该没有）推送【应用挂载目录】

【master机/etc/kasini3000/k4t_master/shoubao_server/【你的应用名】/mnt1/】

------

## 应用（寿报）的功能属性。

* （ax） 从master到node：分发应用的image。1用户pull一个image。2docker save到【master机/etc/kasini3000/k4t_master/shoubao_server/【你的应用名】/image/】

* （ax） 应用中的每个容器，最大生存时间。定时重启，单个，最老，容器。k8s1.15新增手动重启kubectl rollout restart，还不一定是最老，还不能重启单个。

* （mn） 限制每node节点上，container固定副本数。或最小，最大副本数。在master上设定所有node的应用副本数总和。

* （an） 限制container的cpu，内存。支持热更新container的cpu，内存限制。对应docker的--cpus，--memory。

* （m） 限制应用建立在哪些node节点，这可以理解为永久性排斥某应用。

* （a） 设定应用，排斥另外多个应用。node已经有应用a，则本应用b不会运行。

* （m） （这个功能k8s应该没有）设定每应用的负载均衡规则：'在node上平均寿报数量','优先使用内存低的','优先使用cpu低的','按节点id顺序'

* （a） 每应用独立的，挂载目录。 /etc/kasini3000/k4t_node/shoubao/【你的应用名】/mnt1

* （na） （k8s service的lb在摘除pod流量前，单独的等待时间，.spec.terminationGracePeriodSeconds）每应用独立的，寿报下岗等待时间。支持node全局等待时间。

### 每应用独立的，脚本接口（类似于k8s的钩子）

* （a） 探活脚本接口。/etc/kasini3000/k4t_node/shoubao/【你的应用名】/running_check/rc.ps1

------

* （a） docker pull需要的proxy，login，logout脚本接口

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/pull_image_before/docker_login.ps1

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/pull_image_after/docker_logout.ps1

------

* （a） 监控脚本接口。给监控平台返回监控信息。

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/monitor/m.ps1

------

* （a） url注册，反注册接口。反向代理注册，反向代理反注册。

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/url_register/reg.ps1

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/url_unregister/unreg.ps1

------

* （a） dns注册，反注册接口。

**DNS注册特点：有一定的延时**

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/dns_register/reg.ps1

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/dns_unregister/unreg.ps1

------

* （a） 监控脚本接口。根据对container内（连接数）监控信息，变更container副本数。以达到自动扩、缩container副本数量。副本数的扩缩不依赖master。

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/scalable_copies/sc.ps1

------

* （a） 容器启动后，脚本接口。

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/after_start/as.ps1

------

* （a） 容器停止前，脚本接口。

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/before_stop/bt.ps1

------

## 根据外部网关功能，延伸出来的使用方法。

* 基于【外部dns的】蓝绿发布，基于【外部dns的】灰度发布。

* 基于【外部反向代理】的蓝绿发布，基于【外部反向代理】的灰度发布。

* 限流，限速等。

------

## 将来应该永远不会有的功能。

* 不需要类似于k8s的namespace功能。

* 不需要rbac角色功能。轻量的容器管理软件，没必要搞账户和权限。也搞不好。

* 不需要管理ca，tls证书，证书轮换。

* 不需要join master。

* 不用calico，不用flannel，用我独创的node上ip地址池方法。

* 不用istio没有限流。使用外部反代来限流。

* 没有内置dns，但必须要有外部dns。dns算项目外。

* 没有内置反向代理如nginx ingress。但必须使用外部反向代理。反向代理算项目外。

* 没有web图形。k8s不也是只有命令行吗？dashboard是外部项目。欢迎【ASP.NET Core Blazor WebAssembly】开发者前来合作。合作搞k4t的web。

* k4t引擎没有实现crontab。用卡死你3000的crontab引擎可以实现crontab。相当于用master上用定时任务，调用kubectl apply。

* 没有etcd。也不需要apiserver这个rest的http服务器。

* 没有繁琐的yaml。


结论：取其精华，弃其糟粕，是也。

------

# 这个项目怎么样？听我给你吹~~~

* （这个功能k8s应该没有）k4t基于docker，docker支持开启swap，支持容器使用swap。而kubelet和pod不支持swap。

使用ssd硬盘建立swap，可以极大的扩充内存，不管你用云服务器，还是自购服务器，加ssd都比内存便宜。

ssd的swap是对冷数据较多，但占内存巨大的docker容器，比如es，尤其好。


* 脚本中容易保存，计算变量。yaml不行，还得靠额外的脚本代码。

* 脚本灵活，方便自定义。k8s的yaml定义固定。任何使用“yaml关键字定义”的用户，都必须严格遵守【对方的yaml定义】，简称【用户是奴隶】。而任何使用“基于脚本代码的”【用户是主人】。

* 脚本简单，yaml定义繁琐。

* 任何基于yaml东西，的都有个麻烦：“表达foreach麻烦”。 aws工程师提到“YAML是一种很好的格式，用于描述集群的所需状态，但是它没有用于表达逻辑和可重用抽象的原语。” -----这个大家认同吗？

AWS的cdk8s工具包通过使用流行的TypeScript、Python、Java和.NET编程语言来处理框架，从而解决了这个问题。使用这些语言的高级功能，工程师可以编写比YAML支持的更复杂的配置逻辑来自动执行手动任务。这减少了需要编写的样板代码的数量，对于有高级需求的公司来说，它提供了在Kubernetes之上创建多层软件逻辑以抽象其复杂性的能力。

基于powershell的k4t不也是，这样去解决yaml的问题吗？

原文在此： http://www.diglog.com/story/1002026.html


* 没有etcd，帮你省2台机子钱！除os内核外，只需占用不到1gb内存，又帮你省半台机子钱！最新的etcd随机丢数据bug，还无日志。k4t没有

* k8s有时候命名空间删不掉，k4t没有。

* 需要扶梯？不。

* 二进制安装折腾证书？k4t不需要

* 内网的内网，网络插件不稳定？k4t稳定，性能高。

* coredns偶尔解析无返回，基本上必须要安装缓存？k4t不需要

* k4t基于脚本，脚本复制文件目录容易。从master复制【docker映射的路径/mnt】到node，从master分发image到node，从node回收log到master等比k8s容易。
所有镜像只从master上pull然后推到node即可，不香吗？

* k4t支持zabbix监控，可以在k4t集群外。甚至可以是从互联网来的监控。无需在【k4t集群内】安装prometheus，减轻集群负担。降低集群复杂性。

## k4t项目，如何减缓容器的网络存储，这座大山。

目前：

* 从master到node，推送image。

* 从master到node，推送，mnt1，mnt2，目录。


将来：

* 计划设计【c:\ProgramData\kasini3000\k4t_master\shoubao_server\【应用名】\before_start\bs.ps1】

计划从主控机执行复制命令。从nfs到被控机复制文件。

* 计划设计【c:\ProgramData\kasini3000\k4t_master\shoubao_server\【应用名】\after_stop\as.ps1】

计划从主控机执行复制命令。从被控机到nfs复制回文件。


------

# 这个项目缺点是啥？我也不瞒着。

* 当前启动容器的副本总数，需要master定时循环到node上去摘取，汇总。

* master访问node有可能会有超时，失败。（kubelet watch etcd也有可能失败）。访问失败，这是分布式必然存在的问题。

* k4t占用网络资源，若node超过20台，则需要好的路由器，则必须使用vlan技术。




------

## k4t的最大缺点： 必须有的功能，用户必须自己搭建。但是是外部功能，和k4t无关。

dns服务器。用于注册容器。

网关，或反向代理。用户流量转发，限流。建议使用nginx，tengine，kaproxy，kong等。

一篇kong教程： https://www.cnblogs.com/pingyeaa/p/12902454.html


------

## 可有可无的功能。但是是外部功能，和k4t无关。

linux的node上的dns缓存。

------

# 项目由来

最开始的灵感，大概是在2020年4月10日左右出现的。当时我怀疑我是不是疯了？但心底里有个声音告诉我，这是可行的。

5天左右之后，我就开始把这个想法告诉了大家（开吹）。也有很多人觉得我疯了。

其实，不信正常。这是惊天之举，简直让人难以置信。我自己都不是很肯定能搞出来，我自己都不太信我自己。

## 你问：你为什么会开发这个项目？

我答：

* 我也曾夜里坐床学k8s，参悟etcd。但学不会，越学越头昏脑涨，感觉这玩意太麻烦，甚至错了。

* 心底里有个声音告诉我，能成。

* 这个项目能否成功，还是成为笑话，我心里没底，但我想试试。

## 我问：如果k4t项目失败了，我应该被嘲笑吗？

Give Me Some Sunshine给我些阳光

Give Me Some Rain给我些雨露

Give Me Another Chance再给我次机会

let insolent talker 滚！

这世界天生瞧不起你的人太多了，包括每个人的爹妈，他们从你小时候就开始瞧不起你，而不是扶上马送一程。

你不得不从小开始面对鄙视，dissing。而你也继承了“天生瞧不起别人”的这个特性。

* 世界上第一个吃螃蟹的人，应该被人笑是傻瓜吗？

* 世界上第一个怀疑“地球并不是宇宙中心”的人，应该被火烧死吗？

* 世界上第一条上岸探索的鱼，应该被鱼群嘲笑吗？它注定能成功进化出腿，从而适应陆地生活吗？

* 世界上本就没有必然的成功。就算不成功，作为前浪，也给后浪踏出一步探索的路。

![image](可笑至极2.jpg)

中国人总是在外国人基础上修修补补2次开发，缺乏质疑的勇气，和探索的精神。我对k8s的质疑，和探讨，总被视为叛逆，总想要diss我。

想都不敢，那样不是注定被（以美国为首）的外国人，牵着鼻子走吗？

这和  “谁要说地球不是宇宙中心的人，都将被火烧死” 有啥区别？


<!--

disrespectful

An insolent talker

-->

------

# 系统需求

node暂时只支持linux，容器暂时只支持docker。

master支持 :
win10，win2016，win2019，
alpine 3.13，centos7，centos8，
amazon linux 2，麒麟v10高级服务器版x86-64 (Tercel)，统信UOS服务器版2020，Anolis OS 8
Alibaba Cloud Linux2，Alibaba Cloud Linux3，
Rocky Linux 8，AlmaLinux 8，oracle linux server 8，
ubuntu1604，ubuntu1804，ubuntu2004，
debian9，debian10，debian11，
麒麟v10arm，统信UOSarm，安装arm版powershell即可。支持x86，arm架构cpu。

node支持：
centos7，centos8，
amazon linux 2，麒麟v10高级服务器版x86-64 (Tercel)，统信UOS服务器版2020，Anolis OS 8
Alibaba Cloud Linux2，Alibaba Cloud Linux3，
Rocky Linux 8，AlmaLinux 8，oracle linux server 8，
ubuntu1604，ubuntu1804，ubuntu2004，
debian9，debian10，debian11，
麒麟v10arm，统信UOSarm，安装arm版powershell即可。支持x86，arm架构cpu。

不支持：不支持centos6

master上必须安装卡死你3000。     https://gitee.com/chuanjiao10/kasini3000

node上必须安装卡死你3000的agent。     https://gitee.com/chuanjiao10/kasini3000_agent_linux


------

# 程序在哪？

答：

master上的k4t程序，在【k4t_master】目录。

node上的k4t程序，在【k4t_node】目录。

目录内的程序，都是ps1脚本。以源码形式运行。

9月1日后，暂不向大众开源。只向客户开放源码。

## 为什么你要先吹，几个月后，才发布代码？

声明作者是我呀。就怕我先给你代码后，你拿着代码向人吹是你发明的，是你写的。



------

# 名词解释

主控机：master。master上有k4t_master引擎。

linux节点机：node。node上有k4t_node引擎。对标kubelet。

1级数据库（别名：服务器应用）：位于win，linux的master上。建立一个【应用名】的文件夹，内有【k4t.xml】文件。

2级数据库（别名：节点机应用）：位于linux的node上。【shoubao.xml】文件。

3级数据库（别名：寿报）：位于linux的node上。是n个txt文件。每个文件对应一个docker的container，负责单个容器的生命周期。

无双寿报：即副本为1的寿报。即k8s的daemon set。

容器下岗后存活时间：应用的反代反注册后，dns反注册后，则容器下岗。从容器下岗后，到杀死容器前，这段时间就是容器下岗后存活时间。
有两个值，node全局。应用。应用优先。




容器灵：

【道阶法宝】有人听说过吗？这种超级法宝，有一定几率诞生“器灵”。

k4t中，我给容器，发明了“容器灵”！！！用于管理容器。

现有的容器灵有：
* 呼吸机
* 口罩
* 烂香蕉皮
* 臭鸭蛋
* 熔喷布
* 细胞因子风暴
* 烂西瓜
* 苹果核
* 鲱鱼罐头
* 卡苏马苏
* bug
* 喵帝驾崩
* 瑞德韦东

------

# k4t用户手册  document

## master安装：

https://gitee.com/chuanjiao10/k4t/blob/master/docs/master安装_cn.md


## node安装：

https://gitee.com/chuanjiao10/k4t/blob/master/docs/node安装_cn.md

## k4t 旗舰版readme：

https://gitee.com/chuanjiao10/k4t/blob/master/docs/k4t旗舰版readme.md

## k4t日志位置：

* master:         /etc/kasini3000/k4t_master/k4ts_log.txt 或 c:\ProgramData\kasini3000\k4t_master\k4ts_log.txt

* node:         /etc/kasini3000/k4t_node/k4t_log.txt

## 用户应用日志位置：

* master：

/etc/kasini3000/k4t_master/shoubao_server/【你的应用名】/app_log.txt

或

c:\ProgramData\kasini3000\k4t_master\shoubao_server/【你的应用名】/app_log.txt

* node：

/etc/kasini3000/k4t_node/shoubao/【你的应用名】/shoubao_log.txt

## 亲和性调度原理图

![k4t](k4t的寿报node亲和性调度原理图.png)

## 安装设置外部dns --- win

https://gitee.com/chuanjiao10/k4t/blob/master/docs/k4t的ddns.md

## k4t最新消息

https://gitee.com/chuanjiao10/k4t/blob/master/docs/news.md

------

# 关于培训

计划下列培训

1 用户级培训。少许收费。面向k4t用户，k8s用户，docker用户，运维人员。

2 大众讲座。免费。将在k4t官方群内以语音聊天，qq文字聊天方式提供。主要内容为k4t特性，安装卡死你3000，和k8s的对比等。



------

# license

本项目采用自定义license。详见【LICENSE】

This project uses custom LICENSE. The specific LICENSE in this file: LICENSE.

