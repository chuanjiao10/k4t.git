﻿#建议保存编码为：bom头 + utf8
Start-Sleep -Seconds 1

if ($IsLinux -eq $false)
{
	Write-Verbose '错误：系统类型不对。退出码1'
	exit 1
}

$env:LANG = 'zh_CN.UTF-8'

& "${PSScriptRoot}/k4t_client_crontab_写pid.ps1"

& "${PSScriptRoot}/k4t_client_config.ps1"

$private:检查dockerd状态 = & "$PSScriptRoot/___check_dockerd.ps1"
if ($private:检查dockerd状态 -eq $true)
{
}
else
{
	& "$PSScriptRoot/___start_dockerd.ps1"
}

chmod --recursive 755 /etc/kasini3000/k4t_client/shoubao
$Private:所有平安无事文件 = Get-ChildItem -File -Recurse -Path '/etc/kasini3000/k4t_client/shoubao/ping_an_wu_shi.huxiji'
$Private:所有平安无事文件 | ForEach-Object { Remove-Item -LiteralPath $_.fullname -Force }

${private:分钟} = 9,19,29,39,49,59
${private:在9分钟} = $false

${private:iiiiii} = $true
while (${private:iiiiii})
{
	#$Private:所有寿报应用 = Get-ChildItem -File -Recurse -Path '/etc/kasini3000/k4t_client/shoubao/k4t.xml'

	#处理【口罩】问题。
	if (${private:现在}.Second -in $global:k4t_client_副循环间隔在秒)
	{
		$Private:所有寿报应用 = Get-ChildItem -File -Recurse -Path '/etc/kasini3000/k4t_client/shoubao/k4t.xml'
		$Private:所有寿报应用 | ForEach-Object -TimeoutSeconds 150 -ThrottleLimit 200 -Parallel { & '/etc/kasini3000/k4t_client/kou_zhao.ps1' $_.fullname }
	}

	#处理【寿报】问题。
	if (${private:现在}.Second -in $global:k4t_client_主循环间隔在秒)
	{
		$Private:所有寿报应用 = Get-ChildItem -File -Recurse -Path '/etc/kasini3000/k4t_client/shoubao/k4t.xml'
		$Private:所有寿报应用 | ForEach-Object -TimeoutSeconds 150 -ThrottleLimit 200 -Parallel { & '/etc/kasini3000/k4t_client/hu_xi_ji.ps1' $_.fullname }
	}

	# 无事做则休眠，每分钟唤醒。
	if (${private:现在}.Second -in $global:k4t_client_平安无事循环间隔在秒)
	{
		$Private:所有平安无事文件 = Get-ChildItem -File -Recurse -Path '/etc/kasini3000/k4t_client/shoubao/ping_an_wu_shi.huxiji'
		$Private:所有平安无事文件 | ForEach-Object { Remove-Item -LiteralPath $_.fullname -Force }
		#& "${PSScriptRoot}/k4t_锁写log.ps1"  "平安无事文件被删除"
	}

	${private:现在} = Get-Date
	Start-Sleep -Seconds 1

	if (${private:现在}.Minute -in ${private:分钟})
	{
		${private:在9分钟} = $true
	}

	if (${private:在9分钟} -eq $true)
	{
		if (${private:现在}.Second -gt 50)
		{
			${private:iiiiii} = $false
		}
	}
}

if ( (${private:现在}.Hour -eq 23) -and (${private:现在}.Minute -eq 59))
{
	Remove-Item -Force -LiteralPath  '/etc/kasini3000/k4t_client/k4t_log.txt'
}

& "${PSScriptRoot}/k4t_锁写log.ps1"  "信息：k4t client进程正常结束"
& "${PSScriptRoot}/k4t_client_crontab_删pid文件.ps1"
exit 0
