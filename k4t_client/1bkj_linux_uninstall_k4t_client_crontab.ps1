﻿#建议保存编码为：bom头 + utf8

if ($IsLinux -eq $True)
{
	$username = /usr/bin/whoami
	if ($username -ne 'root')
	{
		Write-Error '非管理员'
		Exit 1
	}

	#卸载 k_crontab
	$ubuntu = /usr/bin/cat /etc/*-release | grep ubuntu
	if ( ($ubuntu -eq $null) -or ($ubuntu -eq '') )
	{
		$任务计划文件 = '/var/spool/cron/root'
	}
	else
	{
		$任务计划文件 = '/var/spool/cron/crontabs/root'
	}

	Copy-Item -LiteralPath $任务计划文件 -Destination '/var/spool/' -Force
	$private:新名字 = 'k4t-' + (Get-Date -Format 'yyyy-MM-dd-HH:mm:ss')
	Rename-Item -LiteralPath $任务计划文件 -NewName $private:新名字
	Write-Warning '已经备份任务计划文件，到/var/spool/'
	$所有任务计划 = /usr/bin/crontab -l

	if ($所有任务计划 -match 'k4t_client_crontab.ps1')
	{
		$crontab = Get-Content -LiteralPath $任务计划文件
		$crontab2 = @()
		foreach ($行 in $crontab)
		{
			if ($行.contains('k4t_client_crontab.ps1') )
			{
			}
			else
			{
				$crontab2 += $行
			}
		}
		Set-Content -LiteralPath $任务计划文件 -Value $crontab2 -Encoding UTF8nobom
		chmod 600 $任务计划文件
		systemctl restart cron
		systemctl restart crond
	}
	else
	{
		Write-Error '国产k8s，《kaiiit》容器编排系统，任务计划已经卸载！'
	}
}

Write-Host -ForegroundColor green '国产k8s，《kaiiit》容器编排系统，卸载完成！'
exit 0
