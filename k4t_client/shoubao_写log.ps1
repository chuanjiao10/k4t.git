﻿#建议保存编码为：bom头 + utf8

param
(
	[string]$被写入的log内容 = ''
)

$private:shoubao_log文件路径 = './shoubao_log.txt'

if (Test-Path $private:shoubao_log文件路径)
{
	$private:shoubao_log文件 = Get-Item -LiteralPath $private:shoubao_log文件路径
	if ($private:shoubao_log文件.length -gt 5mb)
	{
		Remove-Item -Force -LiteralPath $private:shoubao_log文件.fullname
	}
}

$private:时间 = Get-Date -Format 'yyyy-MM-dd HH:mm:ss'
$private:输出信息 = "$private:时间	$被写入的log内容"
Add-Content -LiteralPath $private:shoubao_log文件路径 -Value $private:输出信息 -Encoding utf8bom

exit 0
