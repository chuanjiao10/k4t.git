﻿#建议保存编码为：bom头 + utf8

${private:k4t目录} = '/etc/kasini3000/k4t_client'

if (Test-Path -LiteralPath ${private:k4t目录})
{
	${private:k4t_client的pid文件} = "${private:k4t目录}/k4t_client_pid.txt"
	Set-Content -LiteralPath ${private:k4t_client的pid文件} -Value $pid -Encoding utf8BOM -Force
}
else
{
	Write-Error '错误：找不到k4t client 目录'
	exit 1
}

exit 0
