﻿#建议保存编码为：bom头 + utf8
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	$寿报表
)

function 随机延时1
{
	$毫秒1 = 170,170,170,230,230,280,280,310,390,450,560,590
	$延时1 = Get-Random -InputObject $毫秒1
	Write-Verbose "延时 $延时1"
	Start-Sleep -Milliseconds $延时1
}


${Private:ii} = 0
while (${Private:ii} -lt 10)
{
	if (Test-Path -LiteralPath './ping_guo_he.txt')
	{
		随机延时1
	}
	else
	{
		New-Item -ItemType File -Force -Path './ping_guo_he.txt'
		$寿报表 | Export-Clixml -LiteralPath './k4t.xml'
		Remove-Item -Force -LiteralPath './ping_guo_he.txt'
		Write-Host '信息：寿报文件，写入成功。退出码0'
		exit 0
	}

	${Private:ii}++
}

& "${PSScriptRoot}/shoubao_写log.ps1" "错误：寿报文件，写入失败。"
Write-Error '错误：寿报文件，写入失败。退出码2'
exit 2

