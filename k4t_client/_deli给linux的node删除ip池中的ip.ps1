﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$单个ip地址
)

$private:测试结果 = & "$PSScriptRoot/_deli给linux的node删除ip池中的ip.ps1"  $单个ip地址
if ($private:测试结果 -eq $true)
{
	$private:文件名 = "$PSScriptRoot/ip_pool/${单个ip地址}" + '.ipv4'
	Remove-Item -Force -LiteralPath $private:文件名
	exit 0
}
else
{
	Write-Error '错误：删除ip池中的ip失败，退出码1'
	& "${PSScriptRoot}/k4t_锁写log.ps1"  '错误：删除ip池中的ip失败，退出码1'
	exit 1
}


