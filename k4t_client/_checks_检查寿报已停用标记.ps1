﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("name")][String]$应用名
)

$env:LANG = 'zh_CN.UTF-8'
& "${PSScriptRoot}/k4t_client_config.ps1"

try
{
	Set-Location '/etc/kasini3000/k4t_client/shoubao/' -ErrorAction 'stop'
}
catch
{
	Write-Error '错误：找不到shoubao目录。退出码3'
	exit 3
}

if (Test-Path -LiteralPath $应用名)
{
}
else
{
	Write-Error '错误：不存在此应用名。退出码4'
	Set-Location ..
	exit 4
}

Set-Location $应用名

$private:k4t_setting = Import-Clixml -LiteralPath './k4t.xml'

if (${private:k4t_setting}.寿报已经启用 -eq $false)
{
	return $true
}
else
{
	return $false
}
