﻿#建议保存编码为：bom头 + utf8
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("name")][String]$应用名,

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("iname")][String]$image名,

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("iver")][String]$image版本
)

Set-Location '/etc/kasini3000/k4t_client/shoubao/'

try
{
	Set-Location ${应用名} -ErrorAction 'stop'
}
catch
{
	& "${PSScriptRoot}/k4t_锁写log.ps1"  "错误：应用名错误！ ${应用名} ，退出码4"
	exit 4
}

$private:是否有此镜像 = docker image ls | grep ${image名} | grep ${image版本}
if ( ($private:是否有此镜像 -eq $null) -or ($private:是否有此镜像 -eq '') )
{
	Write-Host "信息：正在开始下载【${image名}】的版本【${image版本}】"
}
else
{
	Write-Error "错误：已经有【${image名}】的版本【${image版本}】。退出码1"
	exit 1
}

$private:dp = Start-Process -FilePath '/usr/bin/nohup' -ArgumentList "docker pull ${image名}:${image版本}" -PassThru
Set-Content -LiteralPath './docker_pull_pid.txt' -Value $private:dp.id -Encoding ascii
