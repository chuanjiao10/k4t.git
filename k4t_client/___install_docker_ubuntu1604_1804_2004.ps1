﻿
# https://docs.docker.com/engine/install/ubuntu/
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
[string]$发行版 = lsb_release -cs
[string]$deb字串 = "deb [arch=amd64] https://download.docker.com/linux/ubuntu ${发行版} stable"
sudo add-apt-repository [string]$deb字串
apt-get update
apt-get install docker-ce
systemctl enable docker

$f = '/etc/default/grub'
@(Get-Content $f) -replace 'GRUB_CMDLINE_LINUX=""','GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1"' | Set-Content -Path $f
sudo update-grub
Write-Error '信息：已经更新内核参数，请自行重启linux'

exit 0



