#建议保存编码为：bom头 + utf8

$msg =
@'
问：这个脚本谁写的？有问题找谁技术支持？
答：QQ群号=183173532
名称=powershell交流群

问：4核cpu，最大空闲是多少？
答：400%（win）。（linux）

用法：
$a = hq获取cpu空闲_当前实时值_win-linux通用.ps1
'@

#Write-Warning $msg

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) )
{
	$cpu空闲值_实时_所有核心 = (Get-Counter -Counter  '\Process(idle)\% Processor Time').CounterSamples.CookedValue
	$cpu核心数 = (Get-CimInstance -ClassName  Win32_Processor).NumberOfCores
	$cpu空闲值百分比_实时_所有核心 = ($cpu空闲值_实时_所有核心 / ($cpu核心数 * 100) ).tostring('p')
	Write-Host 'win下，cpu空闲值百分比_实时_所有核心:'
	#write-host $cpu空闲值百分比_实时_所有核心

	#2种返回值，上面屏幕输出百分比，下面给变量返回，小于1的数字。
	$cpu空闲值百分比_实时_所有核心2 = $cpu空闲值_实时_所有核心 / ($cpu核心数 * 100)
	return $cpu空闲值百分比_实时_所有核心2
}

if ($IsLinux -eq $True)
{
	$t1, $t2 = top -b -d 2 -n 2 | grep Cpu
	[decimal]$t11 = $t1.replace('id,', '乂').split('乂')[0].replace('ni,', '乂').split('乂')[1]
	[decimal]$t12 = $t2.replace('id,', '乂').split('乂')[0].replace('ni,', '乂').split('乂')[1]
	#$cpu空闲值百分比_实时_所有核心 = '' + ($t11 + $t12) / 2 + '%'
	#Write-Host 'linux下，cpu空闲值百分比_实时_所有核心:'
	#Write-Host $cpu空闲值百分比_实时_所有核心

	#2种返回值，上面屏幕输出百分比，下面给变量返回，小于1的数字。
	$cpu空闲值百分比_实时_所有核心2 = ($t11 + $t12) / 2
	return $cpu空闲值百分比_实时_所有核心2
}


<#
-b 文字输出
-d 5 间隔5秒
-n 1 1次


top -b -n 1 | grep Cpu
	#$cpu核心数 = grep 'model name' /proc/cpuinfo | wc -l
	#$cpu核心数 = lscpu | grep 'CPU(s):' | head -1 | awk '{print $2}'
	$cpu核心数 = grep -c ^processor /proc/cpuinfo
#>

