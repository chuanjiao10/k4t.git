﻿#建议保存编码为：bom头 + utf8

Start-Sleep -Seconds 1

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Error '错误：不支持win系统，退出码1'
	exit 1
}

& "${PSScriptRoot}/set-pwsh.ps1"

${private:k4t_client的pid文件} = "/etc/kasini3000/k4t_client/k4t_client_pid.txt"
if (Test-Path -LiteralPath ${private:k4t_client的pid文件})
{
	$private:k4t_client_的磁盘pid = & "${PSScriptRoot}/k4t_client_crontab_读pid.ps1"
	if ( ($private:k4t_client_的磁盘pid -eq $null) -or ($private:k4t_client_的磁盘pid -eq '') )
	{
		& "${PSScriptRoot}/k4t_锁写log.ps1"  "错误：没有读取到k4t client进程的pid，这种情况不应该存在。"
		Start-Process -FilePath $global:p_w_s_h_7 -ArgumentList "${PSScriptRoot}/k4t_client.ps1"
	}
	else
	{
		$private:k4t_client_的进程 = Get-Process -Id $private:k4t_client_的磁盘pid
	}

	if ($private:k4t_client_的进程 -eq $null)
	{
		Start-Process -FilePath $global:p_w_s_h_7 -ArgumentList "${PSScriptRoot}/k4t_client.ps1"
	}
	else
	{
		if ( ($private:k4t_client_的进程.ProcessName -eq 'pwsh') -or ($private:k4t_client_的进程.ProcessName -eq 'powershell') )
		{
			${private:k4t_client的pid文件最后写入时间} = Get-Item -LiteralPath ${private:k4t_client的pid文件}
			if ( (Get-Date) -gt (${private:k4t_client的pid文件最后写入时间}.LastWriteTime + [timespan]::FromMinutes(13)) )
			{
				& "${PSScriptRoot}/k4t_锁写log.ps1"  "错误：旧的k4t client进程超时，正在杀死。"
				Stop-Process -Id $private:k4t_client_的进程
				Start-Sleep -Seconds 3
				& "${PSScriptRoot}/k4t_client_crontab_删pid文件.ps1"
				Start-Process -FilePath $global:p_w_s_h_7 -ArgumentList "${PSScriptRoot}/k4t_client.ps1"
			}
		}
	}
}
else
{
	#& "${PSScriptRoot}/k4t_锁写log.ps1"  "信息：新建k4t client进程"
	Start-Process -FilePath $global:p_w_s_h_7 -ArgumentList "${PSScriptRoot}/k4t_client.ps1"
}

