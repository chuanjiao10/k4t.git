﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("name")][String]$应用名
)

$env:LANG = 'zh_CN.UTF-8'
& "${PSScriptRoot}/k4t_client_config.ps1"

Set-Location '/etc/kasini3000/k4t_client/shoubao/'

try
{
	Set-Location ${应用名} -ErrorAction 'stop'
}
catch
{
	& "${PSScriptRoot}/k4t_写log.ps1"  "错误：应用名错误！ ${应用名} ，退出码4"
	exit 4
}

$Private:所有口罩文件 = Get-ChildItem -File -Path './*.kouzhao*'
if (-not ($Private:所有口罩文件 -eq $null) -or ($Private:所有口罩文件 -eq '') )
{
	Write-Error '错误：应用停止完毕才能再次启用。应用正在停止中，请等待。。。退出码1'
	Set-Location ../..
	exit 1
}

$private:k4t_setting = Import-Clixml -LiteralPath './k4t.xml'

if (${private:k4t_setting}.寿报已经启用 -eq $true)
{
	Write-Error '错误：寿报数据库标记，已经是启用。'
}
else
{
	${private:k4t_setting}.寿报已经启用 = $true
	${private:k4t_setting}  | Export-Clixml -LiteralPath './k4t.xml'
}

if (Test-Path -LiteralPath 'get_ipv4_fall.txt')
{
	Remove-Item -Force -LiteralPath 'get_ipv4_fall.txt'
}

if (Test-Path -LiteralPath './!bug!.txt')
{
	Remove-Item -Force -LiteralPath './!bug!.txt'
}

if (Test-Path -LiteralPath './fei_yu_guan_tou.txt')
{
	Remove-Item -Force -LiteralPath './fei_yu_guan_tou.txt'
}

if (Test-Path -LiteralPath './lan_xiang_jiao_pi.txt')
{
	Remove-Item -Force -LiteralPath './lan_xiang_jiao_pi.txt'
}

if (Test-Path -LiteralPath './chou_ya_dan.txt')
{
	Remove-Item -Force -LiteralPath './chou_ya_dan.txt'
}

if (Test-Path -LiteralPath './ping_an_wu_shi.huxiji')
{
	Remove-Item -Force -LiteralPath './ping_an_wu_shi.huxiji'
}

if (Test-Path -LiteralPath './191/miao_di_jia_beng.txt')
{
	Remove-Item -Force -LiteralPath './191/miao_di_jia_beng.txt'
}

if (Test-Path -LiteralPath './docker_pull_pid.txt')
{
	Remove-Item -Force -LiteralPath './docker_pull_pid.txt'
}

if (Test-Path -LiteralPath './shoubao_log.txt')
{
	Remove-Item -Force -LiteralPath './shoubao_log.txt'
}

& "${PSScriptRoot}/_s删除所有呼吸机并重建初始呼吸机.ps1" ${应用名}

Write-Host -ForegroundColor Green '信息：寿报数据库更改成功！请等待1分钟左右后，检查寿报启用状态'

exit 0
