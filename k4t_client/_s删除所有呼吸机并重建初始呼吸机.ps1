﻿#建议保存编码为：bom头 + utf8
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$应用名
)


Set-Location '/etc/kasini3000/k4t_client/shoubao/'

try
{
	Set-Location ${应用名} -ErrorAction 'stop'
}
catch
{
	& "${PSScriptRoot}/k4t_写log.ps1"  "错误：应用名错误！ ${应用名} ，退出码4"
	exit 4
}

$Private:呼吸机 = Get-ChildItem -Path './hu_xi_ji.txt' -File -Recurse
$Private:呼吸机 | Remove-Item -Force
Start-Sleep -Seconds 1
if (Test-Path -LiteralPath './111' -PathType Container)
{
	New-Item -ItemType File -Force -Path './111/hu_xi_ji.txt'
}

Set-Location ../..
exit 0
