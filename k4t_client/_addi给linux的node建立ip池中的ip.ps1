﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$单个ip地址
)

$private:文件名 = "$PSScriptRoot/ip_pool/${单个ip地址}" + '.ipv4'
New-Item -ItemType File -Force -Path $private:文件名
exit 0
