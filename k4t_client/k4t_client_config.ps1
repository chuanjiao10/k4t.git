﻿#建议保存编码为：bom头 + utf8

if ($global:k4t_client_config -eq $true)
{
	exit
}
$global:k4t_client_config = $true

$env:LANG = 'zh_CN.UTF-8'

$global:k4t_client_主循环间隔在秒 = 6,16,26,36,46 #强制杀死寿报延时=20秒。
$global:k4t_client_副循环间隔在秒 = 9,29,49 #强制杀死寿报延时=20秒。
$global:k4t_client_平安无事循环间隔在秒 = 42 #每分钟运行一次。

$global:尝试docker_pull最大次数 = 2
$global:普通寿报_尝试获取ipv4地址最大次数 = 9
$global:docker_pull超时秒 = 300
$global:每node内_尝试启动应用最大次数 = 30
$global:寿报下岗前_等待时间秒_全局 = 20

#超过值。禁止新增寿报。
$global:node最小保留内存_MB = 120 #
$global:node最小保留cpu_百分比 = 5 #

exit 0
