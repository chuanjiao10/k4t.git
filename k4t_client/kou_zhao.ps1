﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("filename")][String]$k4t文件全名,

	[byte]$_本脚本递归次数
)

if ( ($IsWindows -eq $True) -or ($PSVersionTable.psversion.major -lt 6) ) #win
{
	Write-Error '错误：不支持win系统，退出码1'
	exit 1
}

$env:LANG = 'zh_CN.UTF-8'
& "${PSScriptRoot}/k4t_client_config.ps1"

[string]${Private:当前应用名} = $k4t文件全名.
tolower().
replace('/etc/kasini3000/k4t_client/shoubao/','').
replace('/k4t.xml','').
split('/')
#${Private:当前应用名}

Set-Location '/etc/kasini3000/k4t_client/shoubao/'

try
{
	Set-Location ${Private:当前应用名} -ErrorAction 'stop'
}
catch
{
	& "${PSScriptRoot}/k4t_锁写log.ps1"  "错误：应用名错误！ ${Private:当前应用名} ，退出码4"
	exit 4
}

if (Test-Path -LiteralPath './chou_ya_dan.txt')
{
	$Private:temp002 = Get-ChildItem -File -LiteralPath './chou_ya_dan.txt'
	if (((Get-Date) - $Private:temp002.LastWriteTime) -lt [timespan]::FromSeconds(160))
	{
		exit 2
	}
}
else
{
	New-Item -ItemType File -Force -Path './chou_ya_dan.txt'
}

$Private:所有口罩文件 = Get-ChildItem -File -Path './*.kouzhao*'
if ( ($Private:所有口罩文件 -eq $null) -or ($Private:所有口罩文件 -eq '') )
{
	Remove-Item -Force -LiteralPath './chou_ya_dan.txt'
	Set-Location ../..
	exit 3
}

$Private:寿报表 = Import-Clixml -LiteralPath './k4t.xml'

$Private:所有口罩5文件 = Get-ChildItem -File -Path './*.kouzhao5'
if ( ($Private:所有口罩5文件 -eq $null) -or ($Private:所有口罩5文件 -eq '') )
{
	Remove-Item -Force -LiteralPath './an_quan_tou_kui.txt'
}
else
{
	# 立即结束寿报
	$Private:有驱逐点 = & "${PSScriptRoot}/____checkq检查linux的node上的驱逐点.ps1"
	if ( (Test-Path -LiteralPath './!bug!.txt') -or ($Private:有驱逐点 -eq $true) )
	{
		foreach ($Private:当前口罩5文件 in $Private:所有口罩5文件)
		{
			Rename-Item -Force -LiteralPath $Private:当前口罩5文件.fullname -NewName $($Private:当前口罩5文件.basename + '.kouzhao21')
		}
		Remove-Item -Force -LiteralPath './!bug!.txt'
		& "${PSScriptRoot}/shoubao_写log.ps1" "信息: 有驱逐点，或【bug标记】,应用结束。"
	}

	# 滚动更新寿报
	if (Test-Path -LiteralPath './an_quan_tou_kui.txt')
	{
		$Private:滚动更新超时时间 = Get-Date
		foreach ($Private:当前口罩5文件 in $Private:所有口罩5文件)
		{
			if (${Private:寿报表}.每个node内_滚动更新_每个容器时_间隔秒 -gt 10)
			{
				$Private:当前每个node内_滚动更新_每个容器时_间隔秒 = ${Private:寿报表}.每个node内_滚动更新_每个容器时_间隔秒
			}
			else
			{
				$Private:当前每个node内_滚动更新_每个容器时_间隔秒 = 10
			}
			$Private:滚动更新超时时间 = $Private:滚动更新超时时间.AddSeconds($Private:当前每个node内_滚动更新_每个容器时_间隔秒).tostring('yyyy-MM-dd HH:mm:ss')
			Set-Content -Encoding 'utf8BOM' -LiteralPath $Private:当前口罩5文件.fullname -Value $Private:滚动更新超时时间
			Rename-Item -Force -LiteralPath $Private:当前口罩5文件.fullname -NewName $($Private:当前口罩5文件.basename + '.kouzhao7')
		}
		Remove-Item -Force -LiteralPath './an_quan_tou_kui.txt'
	}

	if (Test-Path -LiteralPath './lan_xi_gua.txt')
	{
		foreach ($Private:当前口罩5文件 in $Private:所有口罩5文件)
		{
			Rename-Item -Force -LiteralPath $Private:当前口罩5文件.fullname -NewName $($Private:当前口罩5文件.basename + '.kouzhao6')
		}
		Remove-Item -Force -LiteralPath './lan_xi_gua.txt'
	}
}

foreach ($Private:当前口罩文件 in $Private:所有口罩文件)
{
	$Private:temp001 = $Private:当前口罩文件.basename.tolower().split('_')
	[string]${Private:当前容器id} = $Private:temp001[1]
	[string]${Private:当前容器ip} = $Private:temp001[0]
	#	${Private:当前容器id}
	#	${Private:当前容器ip}

	$Private:当前口罩 = $Private:当前口罩文件.Extension.trimstart('.').tolower()
	switch ($Private:当前口罩)
	{
		'kouzhao1'
		{
			#启动后运行钩子
			if (Test-Path -LiteralPath './after_start/as.ps1')
			{
				& './after_start/as.ps1'
			}
			Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao2')
			$_本脚本递归次数 = 1
			break
		}

		'kouzhao2'
		{
			#启动后探活
			if (Test-Path -LiteralPath './running_check/rc.ps1')
			{
				$Private:探活结果 = & './running_check/rc.ps1' -容器ip地址 ${Private:当前容器ip} -容器端口 ${Private:寿报表}.宿主机端口 -http探活路径 ${Private:寿报表}.http探活路径
				if ($Private:探活结果 -eq $true)
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao3')
					$_本脚本递归次数 = 1
				}
				else
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao16')
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误: 寿报启动后，首次探活失败。${Private:当前容器id} ${Private:当前容器ip}"
				}
			}
			else
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao3')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao3'
		{
			#dns注册
			if (Test-Path -LiteralPath './dns_register/reg.ps1')
			{
				$Private:dns注册结果 = & './dns_register/reg.ps1' ${Private:当前应用名} ${Private:当前容器ip}
				if ($Private:dns注册结果 -eq $true)
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao4')
					$_本脚本递归次数 = 1
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:寿报dns注册失败。${Private:当前容器ip}"
				}
			}
			else
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao4')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao4'
		{
			#url注册
			if (Test-Path -LiteralPath './url_register/reg.ps1')
			{
				$Private:url注册结果 = & './url_register/reg.ps1' ${Private:当前应用名} ${Private:当前容器ip}
				if ($Private:url注册结果 -eq $true)
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao5')
					$_本脚本递归次数 = 1
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:寿报url注册失败。${Private:当前容器ip}"
				}
			}
			else
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao5')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao5'
		{
			$Private:当前容器还在否1 = docker container ls | grep ${Private:当前容器id}
			if ( ($Private:当前容器还在否1 -eq $null) -or ($Private:当前容器还在否1 -eq '') )
			{
				$Private:探测容器是运行状态 = docker container ls -a | grep ${Private:当前容器id}
				if ( ($Private:探测容器是运行状态 -eq $null) -or ($Private:探测容器是运行状态 -eq '') )
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:找不到容器。容器id ${Private:当前容器id} "
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:容器已经停止。 容器id ${Private:当前容器id} "
				}
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao21')
				$_本脚本递归次数 = 1
				break
			}

			if (Test-Path -LiteralPath './fei_yu_guan_tou.txt')
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao21')
				Remove-Item -Force -LiteralPath './fei_yu_guan_tou.txt'
				$_本脚本递归次数 = 1
				break
			}

			#容器运行中探活
			if (Test-Path -LiteralPath './running_check/rc.ps1')
			{
				$Private:探活结果 = & './running_check/rc.ps1' -容器ip地址 ${Private:当前容器ip} -容器端口 ${Private:寿报表}.宿主机端口 -http探活路径 ${Private:寿报表}.http探活路径
				if ($Private:探活结果 -eq $true)
				{

				}
				else
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao21')
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:寿报运行中，探活失败。${Private:当前容器id} ${Private:当前容器ip}"
					$_本脚本递归次数 = 1
				}
			}

			if (Test-Path -LiteralPath './monitor/m.ps1')
			{
				& './monitor/m.ps1' ${Private:当前容器ip}
			}


			if (${Private:寿报表}.应用负载均衡算法 -eq '使用node节点上的sc自动伸缩脚本')
			{
				if (Test-Path -LiteralPath './calable_copies/gc.ps1')
				{
					${Private:口罩5连接数} = & './calable_copies/gc.ps1' ${Private:当前容器ip}
					Set-Content -Encoding unicode -LiteralPath $Private:当前口罩文件 -Value ${Private:口罩5连接数}
				}
			}

			break
		}

		'kouzhao6'
		{
			docker container update --cpus  $(${Private:寿报表}.cpus.split('=')[1]) --memory $(${Private:寿报表}.mem.split([string[]]$('--memory="'))[1].split(' ')[0]) ${Private:当前容器id}
			#https://docs.docker.com/engine/reference/commandline/container_update/

			Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao5')
			break
		}

		'kouzhao7'
		{
			[string]$Private:口罩7字符串 = Get-Content -Raw -LiteralPath $Private:当前口罩文件
			$Private:口罩7日期时间 = $Private:口罩7字符串 -as [datetime]
			if ($Private:口罩7日期时间 -lt (Get-Date))
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao11')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao11'
		{
			if (Test-Path -LiteralPath './url_unregister/unreg.ps1')
			{
				$Private:url反注册结果 = & './url_unregister/unreg.ps1' ${Private:当前应用名} ${Private:当前容器ip}
				if ($Private:url反注册结果 -eq $true)
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao12')
					$_本脚本递归次数 = 1
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:寿报url反注册失败。${Private:当前容器ip}"
				}
			}
			else
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao12')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao21'
		{
			if (Test-Path -LiteralPath './url_unregister/unreg.ps1')
			{
				$Private:url反注册结果 = & './url_unregister/unreg.ps1' ${Private:当前应用名} ${Private:当前容器ip}
				if ($Private:url反注册结果 -eq $true)
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao22')
					$_本脚本递归次数 = 1
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:寿报url反注册失败。${Private:当前容器ip}"
				}
			}
			else
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao22')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao12'
		{
			if (Test-Path -LiteralPath './dns_unregister/unreg.ps1')
			{
				$Private:dns反注册结果 = & './dns_unregister/unreg.ps1' ${Private:当前应用名} ${Private:当前容器ip}
				if ($Private:dns反注册结果 -eq $true)
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao13')
					$_本脚本递归次数 = 1
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:寿报dns反注册失败。${Private:当前容器ip}"
				}
			}
			break
		}

		'kouzhao22'
		{
			if (Test-Path -LiteralPath './dns_unregister/unreg.ps1')
			{
				$Private:dns反注册结果 = & './dns_unregister/unreg.ps1' ${Private:当前应用名} ${Private:当前容器ip}
				if ($Private:dns反注册结果 -eq $true)
				{
					Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao16')
					$_本脚本递归次数 = 1
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误:寿报dns反注册失败。${Private:当前容器ip} "
				}
			}
			else
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao16')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao13'
		{
			Set-Content -Encoding unicode -LiteralPath $Private:当前口罩文件 -Value 'end'
			Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao15')
			break
		}

		'kouzhao15'
		{
			if (${Private:寿报表}.寿报下岗前_等待时间秒 -gt 0)
			{
				$Private:当前超时时长 = ${Private:寿报表}.寿报下岗前_等待时间秒
			}
			else
			{
				$Private:当前超时时长 = $global:寿报下岗前_等待时间秒_全局
			}

			if ($Private:当前超时时长 -gt 255)
			{
				$Private:当前超时时长 = 60
			}

			if ( ${Private:当前口罩文件}.LastWriteTime.AddSeconds($Private:当前超时时长) -lt (Get-Date) )
			{
				Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao16')
				$_本脚本递归次数 = 1
			}
			break
		}

		'kouzhao16'
		{
			if (Test-Path -LiteralPath './before_stop/bt.ps1')
			{
				& './before_stop/bt.ps1'
			}
			docker container stop --time 10 ${Private:当前容器id}
			Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao17')
			break
		}

		'kouzhao17'
		{
			$Private:当前容器还在运行否2 = docker container ls | grep ${Private:当前容器id}
			if ( ($Private:当前容器还在运行否2 -eq $null) -or ($Private:当前容器还在运行否2 -eq '') )
			{
				docker container rm --force ${Private:当前容器id}
			}
			else
			{
				#容器停止失败
				docker container rm --force ${Private:当前容器id}
				Start-Sleep -Seconds 3
				$Private:当前容器还在否3 = docker container ls -a | grep ${Private:当前容器id}
				if ( ($Private:当前容器还在否3 -eq $null) -or ($Private:当前容器还在否3 -eq '') )
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误: 此容器停止失败，但强制删除成功。${Private:当前容器id} "
				}
				else
				{
					& "${PSScriptRoot}/shoubao_写log.ps1" "错误: k4t引擎强制删除容器失败。${Private:当前容器id} "
					& "${PSScriptRoot}/k4t_锁写log.ps1"  "错误: k4t引擎强制删除容器失败。${Private:当前容器id} "
				}
			}
			Rename-Item -Force -LiteralPath $Private:当前口罩文件 -NewName ($Private:当前口罩文件.basename + '.kouzhao18')
			$_本脚本递归次数 = 1
			break
		}

		'kouzhao18'
		{
			New-Item -ItemType File -Force -Path "/etc/kasini3000/k4t_client/ip_pool/${Private:当前容器ip}.ipv4"
			Remove-Item -Force -LiteralPath $Private:当前口罩文件.fullname
			${Private:寿报表}.寿报副本数_本节点_现有 = ${Private:寿报表}.寿报副本数_本节点_现有 - 1
			& "${PSScriptRoot}/_xie写入寿报文件.ps1" -寿报表 ${Private:寿报表}
			break
		}

		default
		{
			& "${PSScriptRoot}/shoubao_写log.ps1" "错误: 错误的口罩！${Private:当前口罩} "
		}
	}
}

Remove-Item -Force -LiteralPath './chou_ya_dan.txt'

if ($_本脚本递归次数 -gt 0)
{
	# & "${PSScriptRoot}/shoubao_写log.ps1" "从 $Private:当前口罩 进入递归"
	$_本脚本递归次数--
	& "$PSCommandPath" -k4t文件全名 $k4t文件全名 -_本脚本递归次数 $_本脚本递归次数
}

Set-Location ../..
exit 0
