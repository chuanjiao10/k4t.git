﻿#建议保存编码为：bom头 + utf8

if ($IsLinux -eq $True)
{
	$username = /usr/bin/whoami
	if ($username -ne 'root')
	{
		Write-Error '非管理员'
		Exit 1
	}

	if ([System.Environment]::Is64BitOperatingSystem -eq $True)
	{

	}
	else
	{
		Write-Error '不支持32位操作系统！'
		Exit 2
	}

	$判断centos6 = @'
rpm -q centos-release
'@ | /usr/bin/bash

	if ( $判断centos6.contains('el6'.tolower()) )
	{
		Write-Error '不支持centos6'
		Exit 3
	}


	if (Test-Path '/etc/kasini3000')
	{

	}
	else
	{
		Write-Error '找不到 /etc/kasini3000'
		Exit 4
	}

	#安装 k_crontab
	$ubuntu = /usr/bin/cat /etc/*-release | grep ubuntu
	if ( ($ubuntu -eq $null) -or ($ubuntu -eq '') )
	{
		$任务计划文件 = '/var/spool/cron/root'
	}
	else
	{
		$任务计划文件 = '/var/spool/cron/crontabs/root'
	}

	Copy-Item -LiteralPath $任务计划文件 -Destination '/var/spool/' -Force
	$private:新名字 = 'k4t-' + (Get-Date -Format 'yyyy-MM-dd-HH:mm:ss')
	Rename-Item -LiteralPath '/var/spool/root' -NewName $private:新名字
	$所有任务计划 = /usr/bin/crontab -l

	if ($所有任务计划 -match 'k4t_client.ps1')
	{
		Write-Error '国产k8s，《kaiiit》容器编排系统，已经安装！'
	}
	else
	{
		$卡死你_crontab_cmd = '* * * * * /usr/bin/pwsh -file /etc/kasini3000/k4t_client/k4t_client_crontab.ps1 > /dev/null 2>&1'
		Add-Content -LiteralPath $任务计划文件 -Value $卡死你_crontab_cmd -Encoding UTF8nobom
		chmod 600 $任务计划文件
		systemctl restart cron
		systemctl restart crond
	}

	chmod --recursive 700 /etc/kasini3000/k4t_client

}
Write-Host -ForegroundColor green '国产k8s，《kaiiit》容器编排系统，安装完成！'
exit 0
