﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("id")][String]$容器id,

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("cmd")][String]$命令

)

$Private:容器获取连接数cmd = @"
docker exec $容器id $命令
"@

$Private:容器获取连接数结果 = Invoke-Expression -Command $Private:容器获取连接数cmd

return $Private:容器获取连接数结果
