#建议保存编码为：bom头 + utf8

$private:存盘文件名 = "${PSScriptRoot}/k4t.ps1"
$private:k4t_setting = Import-Clixml -LiteralPath "${PSScriptRoot}/k4t.xml"
[string]$private:save = @"
`$private:k4t_setting = @{

"@

foreach ($i in $private:k4t_setting.GetEnumerator())
{
	[string]$k = $i.key
	[string]$v = $i.value
	[string]$private:save = $private:save + $k + ' = ' + $v + "`n"
}

$private:save = $private:save + "`n}`n"

Set-Content -LiteralPath $private:存盘文件名 -Value $private:save -Encoding unicode
#out-file -LiteralPath $private:存盘文件名   -inputobject $private:save    -Encoding   unicode

$msg = @'
<#
限制内存：
--memory="300m" --memory-swap="1g" 的含义为：
容器可以使用 300M 的物理内存，并且可以使用 700M(1G -300M) 的 swap。

如果 --memory-swap 的值和 --memory 相同，则容器不能使用 swap。

内存交换文件比例：
--memory-swappiness=""
0----100

cpu限制：
0.1,2.5等。小于1则可以用1核心。

docker run --env VAR1=value1 --env VAR2=value2 ubuntu
不建议热更新环境变量。重启会丢失。
docker exec -it containerId /bin/sh export ENV='value'
#>
'@

Add-Content -LiteralPath $private:存盘文件名 -Value $msg -Encoding unicode
