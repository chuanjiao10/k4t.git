$private:k4t_setting = @{
容器挂载卷1 = -v /etc/kasini3000/k4t_client/shoubao/abc/mnt1:/usr/share/nginx/html:ro
寿报cpu参数 = --cpus=0.5
是无双寿报 = False
无双寿报_ip = 
已经重启过的次数 = 0
宿主机端口 = 80
每个node内_滚动更新每个容器时_间隔秒 = 10
容器挂载卷2 = 
uuid = e8f2b40b-55df-40ea-aea2-2fb6a864e3f8
每node内_尝试启动应用最大次数 = 0
寿报下岗前_等待时间秒 = 12
镜像下载已经出错次数 = 0
寿报已经启用 = True
寿报最大副本数_本节点_设定 = 0
寿报内存参数 = --memory="100m" --memory-swap="300m" --memory-swappiness="20"
有其他寿报的uuid之一_本寿报就不运行 = 
协议 = 
镜像版本 = 1.18.0-alpine
寿报副本数_本节点_现有 = 0
寿报最小副本数_本节点_设定 = 2
寿报副本数_本节点_设定 = 2
镜像名 = nginx
应用已经启用 = True
应用负载均衡算法 = 优先使用内存占用低的node
其他参数 = -d
寿报副本数_所有node节点总和_设定 = 0
寿报副本数_所有node节点总和_现有 = 0
寿报内端口 = 80
环境变量 = 

}

<#
限制内存：
--memory="300m" --memory-swap="1g" 的含义为：
容器可以使用 300M 的物理内存，并且可以使用 700M(1G -300M) 的 swap。

如果 --memory-swap 的值和 --memory 相同，则容器不能使用 swap。

内存交换文件比例：
--memory-swappiness=""
0----100

cpu限制：
0.1,2.5等。小于1则可以用1核心。

docker run --env VAR1=value1 --env VAR2=value2 ubuntu
不建议热更新环境变量。重启会丢失。
docker exec -it containerId /bin/sh export ENV='value'
#>
