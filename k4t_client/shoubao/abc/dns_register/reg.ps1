﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("name")][String]$应用名,

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("ipaddress")][String]$寿报ip地址,

	[String]$ttl = '00:03:00'
)

return $true
# 需要在_建立dns_session文件.ps1内，填写dns服务器的账户密码，然后注释上一行。


$private:建立dns_session文件 = '/etc/kasini3000/k4t_client/_建立dns_session文件.ps1'
if (Test-Path -LiteralPath $private:建立dns_session文件)
{

}
else
{
	Write-Error '错误：找不到建立dns session文件'
	return $false
}

. '/etc/kasini3000/k4t_client/_建立dns_session文件.ps1'

if ( ($private:连接1 -eq $null) -or ($private:连接1 -eq '') )
{
	$private:dns注册结果 = $false
}
else
{
	try
	{
		Invoke-Command -Session $private:连接1 -ScriptBlock {
			Add-DnsServerResourceRecordA -Name $args[0] -IPv4Address $args[1] -TimeToLive $args[2]  -ZoneName v1.k4t
		} -ArgumentList $应用名,$寿报ip地址,$ttl
		$private:dns注册结果 = $true
	}
	catch
	{
		$private:dns注册结果 = $false
	}

	Remove-PSSession -Session $private:连接1
}
return $private:dns注册结果

