﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("ipaddress")][String]$容器ip地址,

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("port")][String]$容器端口,

	[Alias("url_path")][String]$http探活路径
)

$private:探活结果 = $true
return $private:探活结果


# 端口探通例子
$private:端口探通结果 = & "${PSScriptRoot}/dk端口探通.ps1" -目的域名或ip $容器ip地址 -目的端口 $容器端口
if ($private:端口探通结果 -eq $true)
{
	# url探活例子。探活成功应该则return $true。探活失败则应该return $false。探活失败后，容器将被停止。
	& "${PSScriptRoot}/url探活_先慢探活_后快探活.ps1"  "http://${容器ip地址}:${容器端口}${http探活路径}"  #在这里自定义被探活的url
	if ($LASTEXITCODE -eq 0)
	{
		$private:探活结果 = $true
	}
	else
	{
		$private:探活结果 = $false
	}
	return $private:探活结果
}
else
{
	return $private:端口探通结果
}

#命令exec

