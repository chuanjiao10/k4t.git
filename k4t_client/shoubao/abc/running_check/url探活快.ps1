﻿#建议保存编码为：bom头 + utf8

param
(
	#不建议更改这里的参数
	[byte]$探活次数 = 2,
	[byte]$探活间隔秒 = 1,
	[byte]$每次探活超时秒 = 3,
	[string]$目的url = ''
)

if ($目的url -eq '')
{
	return $false
}

for ($i = 0; $i -lt $探活次数; $i++)
{
	$private:每次探活结果 = & "${PSScriptRoot}/url探活.ps1" -目的url探活超时秒 $探活超时秒 -目的url $目的url
	if ($private:每次探活结果 -eq $true)
	{
		$private:最终探活结果++
	}
	Start-Sleep -Seconds $探活间隔秒
}

if ($private:最终探活结果 -ge 1)
{
	return $true
}
else
{
	return $false
}
