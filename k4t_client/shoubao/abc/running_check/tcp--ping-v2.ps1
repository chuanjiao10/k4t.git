﻿# 例子 tcp--ping www.baidu.com 80
param
(
	$MyComputerName = 'www.baidu.com',
	[uint16]$port = '80',
	[switch]$Quiet = $false
)
$tcp对象 = New-Object System.Net.Sockets.TCPClient
$connect = $tcp对象.BeginConnect($MyComputerName, $port, $null, $null)
$wait = $Connect.AsyncWaitHandle.WaitOne(2000, $false)
if ($Quiet -eq $true)
{
	if ($tcp对象.Connected -eq $true)
	{
		$tcp对象.Close()
		$tcp对象.Dispose()
		return $true
	}
	else
	{
		$tcp对象.Close()
		$tcp对象.Dispose()
		return $false
	}
}

exit 0
