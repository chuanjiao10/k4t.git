﻿#建议保存编码为：bom头 + utf8
param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$应用名
)


Set-Location '/etc/kasini3000/k4t_client/shoubao/'

try
{
	Set-Location ${应用名} -ErrorAction 'stop'
}
catch
{
	& "${PSScriptRoot}/k4t_写log.ps1"  "错误：应用名错误！ ${应用名} ，退出码4"
	exit 4
}

$Private:副本数 = Get-ChildItem -Path '*.kouzhao5' -File -Recurse
if ( ($Private:副本数 -eq $null) -or ($Private:副本数 -eq '') )
{
	return 0
}
else
{
	return $Private:副本数.count
}


exit 0
