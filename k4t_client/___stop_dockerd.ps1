﻿
#
& "$PSScriptRoot/k4t_锁写log.ps1" '信息：停止dockerd服务，开始1'

$private:检查dockerd状态 = & "$PSScriptRoot/___check_dockerd.ps1"
if ($private:检查dockerd状态 -eq $true)
{
	& "$PSScriptRoot/k4t_锁写log.ps1" '信息：停止dockerd服务，开始2'

	$private:所有停止容器 = docker ps -a -q
	if ( ($private:所有停止容器 -eq $null) -or ($private:所有停止容器 -eq '') )
	{

	}
	else
	{
		foreach ($private:temp001 in $private:所有停止容器)
		{
			docker rm --force $private:temp001
			Start-Sleep -Seconds 1
		}
	}
	systemctl stop docker
	Start-Sleep -Seconds 2
}
else
{
	& "$PSScriptRoot/k4t_锁写log.ps1" '错误：dockerd服务已经停止。'
}

$private:检查dockerd状态2 = & "$PSScriptRoot/___check_dockerd.ps1"
if ($private:检查dockerd状态2 -eq $true)
{
	& "$PSScriptRoot/k4t_锁写log.ps1" '信息：停止dockerd服务，已经失败'

}
else
{
	& "$PSScriptRoot/k4t_锁写log.ps1" '信息：停止dockerd服务，完毕'
}

exit 0



