﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("name")][String]$应用名
)

$env:LANG = 'zh_CN.UTF-8'
& "${PSScriptRoot}/k4t_client_config.ps1"

try
{
	Set-Location '/etc/kasini3000/k4t_client/shoubao/' -ErrorAction 'stop'
}
catch
{
	Write-Error '错误：找不到shoubao目录。退出码3'
	exit 3
}

if (Test-Path -LiteralPath $应用名)
{
}
else
{
	Write-Error '错误：不存在此应用名。退出码4'
	Set-Location ..
	exit 4
}

Set-Location $应用名

$private:debug = $true
if ($private:debug -eq $true)
{
	$private:呼吸机文件 = Get-ChildItem -Path './hu_xi_ji.txt' -File -Recurse
	if ( ($private:呼吸机文件 -eq $null) -or ($private:呼吸机文件 -eq '') )
	{
		Write-Warning '无呼吸机文件'
	}
	else
	{
		$Private:呼吸机目录 = $Private:呼吸机文件.fullname.
		replace('/hu_xi_ji.txt','').
		replace('/etc/kasini3000/k4t_client/shoubao/','').
		replace(${应用名},'').
		replace('/','')

		Write-Warning "呼吸机=【$Private:呼吸机目录】"
	}
}

$private:副本数 = & "${PSScriptRoot}/_fh返回应用的副本数.ps1" -应用名 $应用名
Write-Warning "应用，正在工作中的，副本数为：【$private:副本数】"


$private:log文件 = "/etc/kasini3000/k4t_client/shoubao/${应用名}/shoubao_log.txt"
Write-Warning "应用log文件位置：${private:log文件}"

if (Test-Path -LiteralPath $private:log文件)
{
	$private:应用最后20条错误记录 = Get-Content -LiteralPath $private:log文件 -Last 20
}
Write-Warning "======应用最后20条错误记录，开始======"
${private:应用最后20条错误记录}
Write-Warning "======应用最后20条错误记录，结束======"
# ----------------------【出错】----------------------
if (Test-Path -LiteralPath './191/miao_di_jia_beng.txt')
{
	Write-Host '信息：应用已经停止！'
	Set-Location ../..
	exit 191
}

if (Test-Path -LiteralPath './201/hu_xi_ji.txt')
{
	Write-Host '信息：node排斥本应用，本应用终止！'
	Set-Location ../..
	exit 201
}

if (Test-Path -LiteralPath './202/hu_xi_ji.txt')
{
	Write-Host '信息：应用报排斥其他应用，本应用终止！'
	Set-Location ../..
	exit 202
}

if (Test-Path -LiteralPath './203/hu_xi_ji.txt')
{
	Write-Host '信息：镜像下载失败，超出最大错误数！'
	Set-Location ../..
	exit 203
}

if (Test-Path -LiteralPath './204/hu_xi_ji.txt')
{
	Write-Host '信息：应用启动次数，超出最大次数！'
	Set-Location ../..
	exit 204
}

if (Test-Path -LiteralPath './205/hu_xi_ji.txt')
{
	Write-Host '信息：ip地址获取失败，超出最大次数！'
	Set-Location ../..
	exit 205
}

if (Test-Path -LiteralPath './206/hu_xi_ji.txt')
{
	Write-Host '信息：无双寿报启动失败！'
	Set-Location ../..
	exit 206
}



# ----------------------【状态】----------------------
$private:寿报启用状态 = & "${PSScriptRoot}/_checks_检查寿报已停用标记.ps1" -应用名 $应用名
if ($private:寿报启用状态 -eq $true)
{
	Write-Host '信息：应用正在停止中！'
	Set-Location ../..
	exit 181
}

if (Test-Path -LiteralPath './113/hu_xi_ji.txt')
{
	Write-Host '信息：应用正在触发滚动更新！'
	Set-Location ../..
	exit 113
}

if (Test-Path -LiteralPath './ping_an_wu_shi.huxiji')
{
	Write-Host '信息：本应用，正在等待最新指令。引擎无事可做，休眠1分钟'
	Set-Location ../..
	exit 301
}

if (Test-Path -LiteralPath './121/hu_xi_ji.txt')
{
	Write-Host '信息：本应用目前工作状态正常，正在计算中。。。'
	Set-Location ../..
	exit 121
}

if (Test-Path -LiteralPath './111/hu_xi_ji.txt')
{
	Write-Host '信息：正在导入镜像，或正在准备下载镜像！'
	Set-Location ../..
	exit 111
}

if (Test-Path -LiteralPath './112/hu_xi_ji.txt')
{
	Write-Host '信息：镜像正在下载中，请等待30秒！'
	Set-Location ../..
	exit 112
}

if (Test-Path -LiteralPath './132/hu_xi_ji.txt')
{
	Write-Host '信息：正在启动无双寿报！'
	Set-Location ../..
	exit 132
}

if (Test-Path -LiteralPath './131/hu_xi_ji.txt')
{
	Write-Host '信息：应用正在获取ip地址！'
	Set-Location ../..
	exit 131
}

if (Test-Path -LiteralPath './133/hu_xi_ji.txt')
{
	Write-Host '信息：正在启动普通应用！检查驱逐点，检查污点，检查cpu，内存剩余量'
	Set-Location ../..
	exit 133
}



Write-Error '错误：应用当前状态未知'
Set-Location ../..
exit 9999
