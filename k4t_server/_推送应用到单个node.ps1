﻿#建议保存编码为：bom头 + utf8

param
(
	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[String]$被推送的应用名,

	[parameter(Mandatory = $true)]
	[ValidateNotNullOrEmpty()]
	[Alias("ipaddress")][String]$node的ip地址
)

if (Test-Path -LiteralPath "${global:kasini3000目录}/k4t_master/shoubao_server/${被推送的应用名}")
{
	Write-Error '错误：找不到被推送的应用名。错误码2'
	exit 2
}

& 'k_copyto_ip.ps1' -目的ip地址 $node的ip地址 -LiteralPath "${global:kasini3000目录}/k4t_master/shoubao_server/${被推送的应用名}" -Destination '/etc/kasini3000/k4t_client/shoubao' -Recurse

exit 0

